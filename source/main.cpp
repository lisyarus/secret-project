#include <SDL2/SDL.h>

#include <secret/map_tools.hpp>
#include <secret/world.hpp>

#include <geom/alias/rectangle.hpp>
#include <geom/operations/matrix.hpp>
#include <geom/operations/rectangle.hpp>
#include <geom/io/vector.hpp>
#include <geom/io/point.hpp>
#include <geom/utility.hpp>
#include <geom/random/vector.hpp>

#include <glow/gl.hpp>
#include <glow/context.hpp>
#include <glow/color.hpp>
#include <glow/camera/spherical.hpp>

#include <glow/object/framebuffer.hpp>
#include <glow/object/program.hpp>
#include <glow/object/vertex_array.hpp>
#include <glow/object/buffer.hpp>

#include <util/clock.hpp>
#include <util/timer.hpp>
#include <util/profiler.hpp>

#include <exception>
#include <iostream>
#include <set>
#include <random>

int main ( ) try
{
	SDL_Init(SDL_INIT_VIDEO);

	SDL_Window * window = SDL_CreateWindow("DEMO1", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800, 600,
		SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE );

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);

	SDL_GLContext context = SDL_GL_CreateContext(window);

	SDL_GL_MakeCurrent(window, context);

	SDL_GL_SetSwapInterval(0);

	glow::load_functions();

	gl::PixelStorei(gl::UNPACK_ALIGNMENT, 1);

	gl::DepthFunc(gl::LEQUAL);

	glow::buffer fullscreen_quad_vbo;
	glow::vertex_array fullscreen_quad_vao;

	{
		geom::point_2i vertices[6]
		{
			{-1, -1},
			{ 1, -1},
			{ 1,  1},
			{-1, -1},
			{ 1,  1},
			{-1,  1},
		};

		fullscreen_quad_vbo.bind_scoped(gl::ARRAY_BUFFER)
			.data(vertices)
			.with([&]{
				fullscreen_quad_vao.bind_scoped()
					.attrib(0, 2, gl::INT, gl::FALSE_, 0, nullptr);
			});
	}

	const char postprocessing_vs[] =
R"(#version 330

layout (location = 0) in vec2 in_position;

out vec2 texcoord;

void main ( )
{
	gl_Position = vec4(in_position, 0.0, 1.0);

	texcoord = (vec2(1.0, 1.0) + in_position) * 0.5;
}
)";

	const char postprocessing_fs[] =
R"(#version 330

uniform sampler2D u_color_texture;
uniform sampler2D u_normal_texture;
uniform sampler2D u_position_texture;
uniform sampler2D u_depth_texture;
uniform vec2 u_depth_range;
uniform vec2 u_pixel_size;
uniform vec3 u_world_size;

in vec2 texcoord;

out vec4 out_color;

float linear_z (float z)
{
	float n = u_depth_range.x;
	float f = u_depth_range.y;
	return 2.0 * n / (f + n - z * (f - n));
}

void main ( )
{
	vec4 color = texture(u_color_texture, texcoord);

	if (color.a < 0.5)
	{
		vec4 cx0 = texture(u_color_texture, texcoord - vec2(u_pixel_size.x, 0.0));
		vec4 cx1 = texture(u_color_texture, texcoord + vec2(u_pixel_size.x, 0.0));
		vec4 cy0 = texture(u_color_texture, texcoord - vec2(0.0, u_pixel_size.y));
		vec4 cy1 = texture(u_color_texture, texcoord + vec2(0.0, u_pixel_size.y));

//		out_color = (cx0 + cx1 + cy0 + cy1) / 4.0;
//		return;

		int i = 0;

		if (cx0.a < 0.5)
			++i;

		if (cx1.a < 0.5)
			++i;

		if (cy0.a < 0.5)
			++i;

		if (cy1.a < 0.5)
			++i;

		if (i == 0)
		{
			// T-junction removal

			out_color = (cx0 + cx1 + cy0 + cy1) / 4.0;
			return;
		}
	}

	vec3 n = texture(u_normal_texture, texcoord).xyz;
	vec3 nx0 = texture(u_normal_texture, texcoord - vec2(u_pixel_size.x, 0.0)).xyz;
	vec3 nx1 = texture(u_normal_texture, texcoord + vec2(u_pixel_size.x, 0.0)).xyz;
	vec3 ny0 = texture(u_normal_texture, texcoord - vec2(0.0, u_pixel_size.y)).xyz;
	vec3 ny1 = texture(u_normal_texture, texcoord + vec2(0.0, u_pixel_size.y)).xyz;

	float ng = length(n - nx0) + length(n - nx1) + length(n - ny0) + length(n - ny1);

	if (ng > 0.25)
	{
		// Normal discontinuity

		out_color = vec4(0.0, 0.0, 0.0, 1.0);
		return;
	}

	n = (n - vec3(0.5, 0.5, 0.5)) / 0.5;

	float z = linear_z(texture(u_depth_texture, texcoord).r);

	vec3 px0 = texture(u_position_texture, texcoord - vec2(u_pixel_size.x, 0.0)).xyz * u_world_size;
	vec3 px1 = texture(u_position_texture, texcoord + vec2(u_pixel_size.x, 0.0)).xyz * u_world_size;
	vec3 py0 = texture(u_position_texture, texcoord - vec2(0.0, u_pixel_size.y)).xyz * u_world_size;
	vec3 py1 = texture(u_position_texture, texcoord + vec2(0.0, u_pixel_size.y)).xyz * u_world_size;

	vec3 gx = normalize(px1 - px0);
	vec3 gy = normalize(py1 - py0);

	float g = (abs(dot(n, gx)) + abs(dot(n, gy)));

	if (g > 0.03)
		out_color = vec4(0.0, 0.0, 0.0, 1.0);
	else
		out_color = color;
}
)";

	glow::program postprocessing_program(
		glow::vertex_shader(postprocessing_vs),
		glow::fragment_shader(postprocessing_fs));

	glow::texture postprocessing_color;
	glow::texture postprocessing_depth;
	glow::texture postprocessing_normal;
	glow::texture postprocessing_position;
	glow::framebuffer postprocessing_framebuffer;

	postprocessing_color.bind_scoped(gl::TEXTURE_2D)
		.parameter(gl::TEXTURE_MIN_FILTER, gl::NEAREST)
		.parameter(gl::TEXTURE_MAG_FILTER, gl::NEAREST)
		.parameter(gl::TEXTURE_WRAP_S, gl::CLAMP_TO_EDGE)
		.parameter(gl::TEXTURE_WRAP_T, gl::CLAMP_TO_EDGE);

	postprocessing_depth.bind_scoped(gl::TEXTURE_2D)
		.parameter(gl::TEXTURE_MIN_FILTER, gl::NEAREST)
		.parameter(gl::TEXTURE_MAG_FILTER, gl::NEAREST)
		.parameter(gl::TEXTURE_WRAP_S, gl::CLAMP_TO_EDGE)
		.parameter(gl::TEXTURE_WRAP_T, gl::CLAMP_TO_EDGE);

	postprocessing_normal.bind_scoped(gl::TEXTURE_2D)
		.parameter(gl::TEXTURE_MIN_FILTER, gl::NEAREST)
		.parameter(gl::TEXTURE_MAG_FILTER, gl::NEAREST)
		.parameter(gl::TEXTURE_WRAP_S, gl::CLAMP_TO_EDGE)
		.parameter(gl::TEXTURE_WRAP_T, gl::CLAMP_TO_EDGE);

	postprocessing_position.bind_scoped(gl::TEXTURE_2D)
		.parameter(gl::TEXTURE_MIN_FILTER, gl::NEAREST)
		.parameter(gl::TEXTURE_MAG_FILTER, gl::NEAREST)
		.parameter(gl::TEXTURE_WRAP_S, gl::CLAMP_TO_EDGE)
		.parameter(gl::TEXTURE_WRAP_T, gl::CLAMP_TO_EDGE);

	secret::world world({512, 128, 512}, *secret::make_default_world_generator());

	secret::map_tools tools(world);

	glow::spherical_camera camera;

	camera.fov_x = M_PI / 4.f;
	camera.aspect_ratio = 1.f;
	camera.near_clip = 2.0f;
	camera.far_clip = 1024.f;

	camera.azimuthal_angle = M_PI / 3;
	camera.elevation_angle = M_PI / 4;
	camera.distance = 128.f;
	camera.target = { world.size()[0] / 2.f, world.size()[1] / 2.f, world.size()[2] / 2.f };

	float camera_distance_dst = camera.distance;
	float camera_azimuthal_angle_dst = camera.azimuthal_angle;
	float camera_elevation_angle_dst = camera.elevation_angle;
	geom::point_3f camera_target_dst = camera.target;

	geom::interval<float> const camera_distance_range {8.f, 256.f};

	bool left_button_down = false;
	bool middle_button_down = false;
	bool right_button_down = false;

	geom::point_2i mouse_pos;

	geom::vector_2i screen_size;

	enum class mouse_action_t
	{
		put,
		erase,
	} mouse_action = mouse_action_t::put;

	enum class mouse_mode_t
	{
		light,
		sand,
	} mouse_mode = mouse_mode_t::light;

	int mouse_action_radius = 0;

	util::clock<std::chrono::duration<float>> frame_clock;

	std::chrono::milliseconds const physics_tick_period { 50 };

	using physics_clock = std::chrono::high_resolution_clock;
	physics_clock::time_point last_physics_tick = physics_clock::now();

	std::size_t physics_tick_count = 0;

	std::size_t frame_count = 0;

	for (bool running = true; running;)
	{
		std::optional<util::profiler> frame_profiler;

		if ((frame_count % 64) == 0)
			frame_profiler.emplace("Frame");

		float const dt = frame_clock.restart().count();

		bool had_right_button_up = false;
		bool had_left_button_up = false;
		bool had_left_button_down = false;

		for (SDL_Event event; SDL_PollEvent(&event);) switch (event.type)
		{
		case SDL_QUIT:
			running = false;
			break;
		case SDL_KEYDOWN:
			switch (event.key.keysym.sym)
			{
			case SDLK_ESCAPE:
				running = false;
				break;
			case SDLK_SPACE:
				if (mouse_action == mouse_action_t::put)
					mouse_action = mouse_action_t::erase;
				else
					mouse_action = mouse_action_t::put;
				break;
			case SDLK_KP_ENTER:
				if (mouse_mode == mouse_mode_t::light)
					mouse_mode = mouse_mode_t::sand;
				else
					mouse_mode = mouse_mode_t::light;
				break;
			case SDLK_KP_PLUS:
				mouse_action_radius += 1;
				break;
			case SDLK_KP_MINUS:
				mouse_action_radius -= 1;
				if (mouse_action_radius < 0)
					mouse_action_radius = 0;
				break;
			}
			break;
		case SDL_WINDOWEVENT:
			switch (event.window.event)
			{
			case SDL_WINDOWEVENT_RESIZED:
				screen_size = { event.window.data1, event.window.data2 };
				gl::Viewport(0, 0, event.window.data1, event.window.data2);
				camera.aspect_ratio = static_cast<float>(event.window.data1) / event.window.data2;

				postprocessing_color.bind_scoped(gl::TEXTURE_2D)
					.image_2d(0, screen_size, (glow::color_4ub const *)(nullptr));

				postprocessing_depth.bind_scoped(gl::TEXTURE_2D)
					.image_2d(0, gl::DEPTH_COMPONENT24, screen_size, gl::DEPTH_COMPONENT, gl::UNSIGNED_INT, nullptr);

				postprocessing_normal.bind_scoped(gl::TEXTURE_2D)
					.image_2d(0, screen_size, (glow::color_3ub const *)(nullptr));

				postprocessing_position.bind_scoped(gl::TEXTURE_2D)
					.image_2d(0, gl::RGBA32UI, screen_size, gl::RGBA_INTEGER, gl::UNSIGNED_INT, nullptr);

				postprocessing_framebuffer.bind_scoped()
					.attach_color(postprocessing_color,    0)
					.attach_color(postprocessing_normal,   1)
					.attach_color(postprocessing_position, 2)
					.draw_buffers(gl::COLOR_ATTACHMENT0, gl::COLOR_ATTACHMENT1, gl::COLOR_ATTACHMENT2)
					.attach_depth(postprocessing_depth)
					.assert_complete();

				break;
			}
			break;
		case SDL_MOUSEBUTTONDOWN:
			switch (event.button.button)
			{
			case SDL_BUTTON_LEFT:
				left_button_down = true;
				had_left_button_down = true;
				break;
			case SDL_BUTTON_MIDDLE:
				middle_button_down = true;
				break;
			case SDL_BUTTON_RIGHT:
				right_button_down = true;
				break;
			}
			break;
		case SDL_MOUSEBUTTONUP:
			switch (event.button.button)
			{
			case SDL_BUTTON_LEFT:
				left_button_down = false;
				had_left_button_up = true;
				break;
			case SDL_BUTTON_MIDDLE:
				middle_button_down = false;
				break;
			case SDL_BUTTON_RIGHT:
				right_button_down = false;
				had_right_button_up = true;
				break;
			}
			break;
		case SDL_MOUSEMOTION:
			if (middle_button_down)
			{
				float const angle_change_speed = 2.f * geom::pi_f / screen_size[0];

				camera_azimuthal_angle_dst += angle_change_speed * event.motion.xrel;
				camera_elevation_angle_dst += angle_change_speed * event.motion.yrel;
			}
			camera_elevation_angle_dst = geom::clamp(camera_elevation_angle_dst, {-geom::pi_f / 2.f, geom::pi_f / 2.f});
			mouse_pos = { event.motion.x, event.motion.y };
			break;
		case SDL_MOUSEWHEEL:
			camera_distance_dst *= std::pow(0.8f, event.wheel.y);
			camera_distance_dst = geom::clamp(camera_distance_dst, camera_distance_range);
			break;
		}

		if (!running)
			break;

		{
			float d = dt;
			if (d > 0.05f)
				d = 0.05f;
			camera.target += (camera_target_dst - camera.target) * 10.f * d;
			camera.azimuthal_angle += (camera_azimuthal_angle_dst - camera.azimuthal_angle) * 20.f * d;
			camera.elevation_angle += (camera_elevation_angle_dst - camera.elevation_angle) * 20.f * d;
			camera.distance += (camera_distance_dst - camera.distance) * 20.f * d;
		}

		auto const camera_modelview = camera.view();
		auto const camera_projection = camera.projection();

		auto const camera_transform = camera.transform();
		auto const camera_transform_inv = geom::inverse(camera_transform);

		auto const camera_clip_planes = camera.clip_planes();

		auto const camera_pos = camera.position();

		geom::vector_2f const mouse_pos_ndc =
		{
			(2.f * mouse_pos[0]) / screen_size[0] - 1.f,
			1.f - (2.f * mouse_pos[1]) / screen_size[1],
		};

		auto const mouse_dir_homogeneous = camera_transform_inv * geom::vector_4f{ mouse_pos_ndc[0], mouse_pos_ndc[1], -1.f, 1.f };

		geom::vector_3f const mouse_dir =
		{
			mouse_dir_homogeneous[0] / mouse_dir_homogeneous[3] - camera_pos[0],
			mouse_dir_homogeneous[1] / mouse_dir_homogeneous[3] - camera_pos[1],
			mouse_dir_homogeneous[2] / mouse_dir_homogeneous[3] - camera_pos[2],
		};

		secret::world::raycast_result const raycast_result = world.raycast({ camera_pos, mouse_dir });

		if (auto terrain_hit = std::get_if<secret::world::terrain_hit>(&raycast_result))
		{
			if (had_right_button_up)
			{
				camera_target_dst = geom::cast<float>(terrain_hit->block) + geom::point_3f::zero();

//				camera_target_dst += geom::vector_3f{0.5f, 0.5f, 0.5f} + 0.5f * face_normal(selection->face);
				camera_target_dst += geom::vector_3f{0.5f, 0.5f, 0.5f};
			}

			if (mouse_action == mouse_action_t::put)
			{
				geom::vector_3ul new_block = terrain_hit->block + geom::cast<std::size_t>(secret::face_normal_i(terrain_hit->face));

				tools.set_tool(secret::map_tools::place_block{new_block, secret::block::stone, mouse_action_radius});
			}
			else
			{
				tools.set_tool(secret::map_tools::remove_block{terrain_hit->block, mouse_action_radius});
			}
		}
		else
		{
			tools.set_tool(secret::map_tools::no_tool{});
		}

		if (had_left_button_down)
		{
			tools.execute();
		}
/*
		auto sqr = [](auto x){ return x * x; };

		if (current_selection)
		{
			if (had_left_button_down)
			{
				for (int dz = -mouse_action_radius; dz <= mouse_action_radius; ++dz)
				{
					for (int dy = -mouse_action_radius; dy <= mouse_action_radius; ++dy)
					{
						for (int dx = -mouse_action_radius; dx <= mouse_action_radius; ++dx)
						{
							if (sqr(dx) + sqr(dy) + sqr(dz) <= sqr(mouse_action_radius))
							{
								if (mouse_mode == mouse_mode_t::light)
								{
									if (mouse_action == mouse_action_t::put)
									{
										auto b = geom::cast<std::size_t>(geom::cast<int>(current_selection->block) + secret::face_normal_i(current_selection->face) + geom::vector_3i{dx, dy, dz});

										if (b[0] < world_size[0] * secret::chunk_size
											&& b[1] < world_size[1] * secret::chunk_size
											&& b[2] < world_size[2] * secret::chunk_size
											&& (terrain.get(b[0], b[1], b[2]) == secret::block::air))
										{
											terrain.set_light(b[0], b[1], b[2]);
										}
									}

									if (mouse_action == mouse_action_t::erase)
									{
										auto b = geom::cast<std::size_t>(geom::cast<int>(current_selection->block) + secret::face_normal_i(current_selection->face) + geom::vector_3i{dx, dy, dz});

										if (b[0] < world_size[0] * secret::chunk_size
											&& b[1] < world_size[1] * secret::chunk_size
											&& b[2] < world_size[2] * secret::chunk_size
											&& (terrain.get(b[0], b[1], b[2]) == secret::block::air))
										{
											terrain.remove_light(b[0], b[1], b[2]);
										}
									}
								}

								if (mouse_mode == mouse_mode_t::sand)
								{
									if (mouse_action == mouse_action_t::put)
									{
										auto b = geom::cast<std::size_t>(geom::cast<int>(current_selection->block) + secret::face_normal_i(current_selection->face) + geom::vector_3i{dx, dy, dz});

										if (b[0] < world_size[0] * secret::chunk_size
											&& b[1] < world_size[1] * secret::chunk_size
											&& b[2] < world_size[2] * secret::chunk_size
											&& (terrain.get(b[0], b[1], b[2]) == secret::block::air))
										{
											terrain.set(b[0], b[1], b[2], secret::block::stone);
										}
									}

									if (mouse_action == mouse_action_t::erase)
									{
										auto b = geom::cast<std::size_t>(geom::cast<int>(current_selection->block) + geom::vector_3i{dx, dy, dz});

										if (b[0] < world_size[0] * secret::chunk_size
											&& b[1] < world_size[1] * secret::chunk_size
											&& b[2] < world_size[2] * secret::chunk_size
											&& (terrain.get(b[0], b[1], b[2]) != secret::block::air))
										{
											terrain.set(b[0], b[1], b[2], secret::block::air);
										}
									}
								}
							}
						}
					}
				}
			}
		}
*/
		if (auto now = physics_clock::now(); (now - last_physics_tick) > physics_tick_period)
		{
			float const dt = std::chrono::duration_cast<std::chrono::duration<float>>(now - last_physics_tick).count();
			world.step(physics_tick_count++, dt);
			last_physics_tick = now;
		}

		postprocessing_framebuffer.bind_scoped(gl::DRAW_FRAMEBUFFER)
			.with([&]{
				glow::clear_color(geom::vector_4f{0.75f, 0.75f, 1.f, 0.f});
				gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);

				world.render({camera_modelview, camera_projection, camera_pos, camera_target_dst, camera_clip_planes});

				tools.render(camera_transform);
			});

		postprocessing_color.bind_scoped(gl::TEXTURE_2D)
			.with([&]{
				gl::ActiveTexture(gl::TEXTURE1);
				postprocessing_normal.bind_scoped(gl::TEXTURE_2D)
					.with([&]{
						gl::ActiveTexture(gl::TEXTURE2);
						postprocessing_position.bind_scoped(gl::TEXTURE_2D)
							.with([&]{
								gl::ActiveTexture(gl::TEXTURE3);
								postprocessing_depth.bind_scoped(gl::TEXTURE_2D)
									.with([&]{
										postprocessing_program.bind_scoped()
											.uniform("u_color_texture", std::nothrow, 0)
											.uniform("u_normal_texture", std::nothrow, 1)
											.uniform("u_position_texture", std::nothrow, 2)
											.uniform("u_depth_texture", std::nothrow, 3)
											.uniform("u_depth_range", std::nothrow, geom::vector_2f{camera.near_clip, camera.far_clip})
											.uniform("u_pixel_size", std::nothrow, geom::vector_2f{1.f / screen_size[0], 1.f / screen_size[1]})
											.uniform("u_world_size", std::nothrow, geom::cast<float>(world.size()))
											.with([&]{
												fullscreen_quad_vao.bind_scoped().draw(gl::TRIANGLES, 0, 6);
											});
									});
								gl::ActiveTexture(gl::TEXTURE2);
							});
						gl::ActiveTexture(gl::TEXTURE1);
					});
				gl::ActiveTexture(gl::TEXTURE0);
			});

		SDL_GL_SwapWindow(window);

		++frame_count;
	}
}
catch (std::exception const & ex)
{
	std::cerr << ex.what() << '\n';
	return 1;
}
