#include <secret/perlin.hpp>

#include <geom/random/vector.hpp>

namespace secret
{

	static float smoothstep (float t)
	{
		return t * t * (3.f - 2.f * t);
	}

	geom::ndarray<float, 2> perlin (std::default_random_engine & rng,
		geom::vector_2ul const & block_count, geom::vector_2ul const & block_size,
		bool repeatable)
	{
		geom::ndarray<geom::vector_2f, 2> perlin_map({block_count[0] + 1, block_count[1] + 1});

		geom::random::uniform_sphere_vector_distribution<geom::vector_2f> random_circle;

		for (std::size_t y = 0; y < block_count[1]; ++y)
		{
			for (std::size_t x = 0; x < block_count[0]; ++x)
			{
				perlin_map(x, y) = random_circle(rng);
			}
		}

		if (repeatable)
		{
			for (std::size_t x = 0; x < block_count[0]; ++x)
			{
				perlin_map(x, block_count[1]) = perlin_map(x, 0ul);
			}

			for (std::size_t y = 0; y < block_count[1]; ++y)
			{
				perlin_map(block_count[0], y) = perlin_map(0ul, y);
			}

			perlin_map(block_count[0], block_count[1]) = perlin_map(0ul, 0ul);
		}
		else
		{
			for (std::size_t x = 0; x < block_count[0]; ++x)
			{
				perlin_map(x, block_count[1]) = random_circle(rng);
			}

			for (std::size_t y = 0; y < block_count[1]; ++y)
			{
				perlin_map(block_count[0], y) = random_circle(rng);
			}

			perlin_map(block_count[0], block_count[1]) = random_circle(rng);
		}

		geom::ndarray<float, 2> result({block_count[0] * block_size[0], block_count[1] * block_size[1]});

		for (std::size_t y = 0; y < block_count[1] * block_size[1]; ++y)
		{
			for (std::size_t x = 0; x < block_count[0] * block_size[0]; ++x)
			{
				std::size_t const x0 = x / block_size[0];
				std::size_t const x1 = x0 + 1;
				std::size_t const y0 = y / block_size[1];
				std::size_t const y1 = y0 + 1;

				float const tx = static_cast<float>(x) / block_size[0] - x0;
				float const ty = static_cast<float>(y) / block_size[1] - y0;

				geom::vector_2f const v00 { tx      , ty       };
				geom::vector_2f const v10 { tx - 1.f, ty       };
				geom::vector_2f const v01 { tx      , ty - 1.f };
				geom::vector_2f const v11 { tx - 1.f, ty - 1.f };

				float const d00 = v00 * perlin_map(x0, y0);
				float const d10 = v10 * perlin_map(x1, y0);
				float const d01 = v01 * perlin_map(x0, y1);
				float const d11 = v11 * perlin_map(x1, y1);

				float const sx = smoothstep(tx);
				float const sy = smoothstep(ty);

				result(x, y) =
					(d00 * (1.f - sx) + d10 * sx) * (1.f - sy) +
					(d01 * (1.f - sx) + d11 * sx) * sy;
			}
		}

		return result;
	}

}
