#include <secret/grass.hpp>

#include <glow/object/buffer.hpp>
#include <glow/object/vertex_array.hpp>
#include <glow/object/program.hpp>
#include <glow/object/texture.hpp>
#include <glow/color.hpp>

#include <geom/utility.hpp>
#include <geom/random/vector.hpp>
#include <geom/primitives/ndarray.hpp>

#include <random>

namespace
{

	const char grass_program_vertex_source[] =
R"(#version 330

uniform mat4 u_transform;

layout (location = 0) in vec3 in_position;
layout (location = 1) in vec2 in_texcoord;
layout (location = 2) in vec3 in_origin;
layout (location = 3) in int in_rotation;
layout (location = 4) in vec3 in_blade_origin;
layout (location = 5) in float in_size;

out vec4 color;
out vec2 texcoord;

const mat2 rotations[8] = mat2[8](
	mat2( 1.0,  0.0,  0.0,  1.0),
	mat2(-1.0,  0.0,  0.0,  1.0),
	mat2( 1.0,  0.0,  0.0, -1.0),
	mat2(-1.0,  0.0,  0.0, -1.0),
	mat2( 0.0,  1.0,  1.0,  0.0),
	mat2( 0.0, -1.0,  1.0,  0.0),
	mat2( 0.0,  1.0, -1.0,  0.0),
	mat2( 0.0, -1.0, -1.0,  0.0)
);

vec3 rotate (vec3 p, int r)
{
	vec2 q = rotations[r] * p.xz;
	return vec3(q.x, p.y, q.y);
}

void main ( )
{
	vec3 p = in_position;
	vec3 o = in_blade_origin;
	p.y *= 2.0;
	o.y *= 2.0;
	p = (p - o) * in_size + o;
	gl_Position = u_transform * vec4(in_origin + rotate(p, in_rotation) + vec3(0.5, 2.0, 0.5), 1.0);

	texcoord = in_texcoord;
}
)";

	const char grass_program_fragment_source[] =
R"(#version 330

uniform sampler2D u_texture;

in vec2 texcoord;

out vec4 out_color;

void main ( )
{
	out_color = texture(u_texture, texcoord);
}
)";

	struct vertex
	{
		geom::point_3b position;
		geom::point_3b blade_origin;
		geom::vector_2ub texcoord;
	};

	struct instance
	{
		geom::point_3s origin;
		unsigned char rotation;
		unsigned char size;
	};

	auto const attribs = glow::interleaved<glow::normalized<geom::point_3b>, glow::normalized<geom::point_3b>, glow::normalized<geom::vector_2ub>>();

	auto const instance_attribs = glow::interleaved<geom::point_3s, unsigned char, glow::normalized<unsigned char>>();

	static_assert(sizeof(vertex) == 8);
	static_assert(sizeof(instance) == 8);

	glow::texture make_grass_texture ( )
	{
		std::size_t const texture_size = 32;

		geom::ndarray<glow::color_4ub, 2> pixels({texture_size, texture_size});

		glow::color_3f c00 { 0.f, 0.25f, 0.f };
		glow::color_3f c10 { 0.25f, 0.25f, 0.f };
		glow::color_3f c01 { 0.f, 0.75f, 0.f };
		glow::color_3f c11 { 0.5f, 0.75f, 0.f };

		for (std::size_t u = 0; u < texture_size; ++u)
		{
			for (std::size_t v = 0; v < texture_size; ++v)
			{
				float const t = float(u) / texture_size;
				float const s = float(v) / texture_size;

				auto c = c00 * (1-t) * (1-s) + c10 * t * (1-s) + c01 * (1-t) * s + c11 * t * s;

				pixels(u, v)[0] = (unsigned char)(c[0] * 255.f);
				pixels(u, v)[1] = (unsigned char)(c[1] * 255.f);
				pixels(u, v)[2] = (unsigned char)(c[2] * 255.f);
				pixels(u, v)[3] = 255;
			}
		}

		glow::texture grass_texture;

		grass_texture.bind_scoped(gl::TEXTURE_2D)
			.parameter(gl::TEXTURE_MAG_FILTER, gl::NEAREST)
			.parameter(gl::TEXTURE_MIN_FILTER, gl::NEAREST)
			.parameter(gl::TEXTURE_BASE_LEVEL, 0)
			.parameter(gl::TEXTURE_MAX_LEVEL, 0)
			.parameter(gl::TEXTURE_WRAP_S, gl::CLAMP_TO_EDGE)
			.parameter(gl::TEXTURE_WRAP_T, gl::CLAMP_TO_EDGE)
			.image_2d(0, {texture_size, texture_size}, pixels.data());

		return grass_texture;
	}

}

namespace secret
{

	struct grass::implementation
	{
		glow::buffer vbo;
		glow::buffer index_vbo;
		glow::buffer instance_vbo;
		glow::vertex_array vao;
		glow::program program;
		glow::texture texture;

		std::size_t indices_count;
		std::size_t instances_count;

		implementation (block_map const & map);
	};

	grass::implementation::implementation (block_map const & map)
		: program(
			glow::vertex_shader(grass_program_vertex_source),
			glow::fragment_shader(grass_program_fragment_source))
		, texture(make_grass_texture())
	{
		std::vector<vertex> vertices;
		std::vector<unsigned int> indices;

		std::vector<instance> instances;

		std::default_random_engine rng{std::random_device{}()};

		auto push_vertex = [&](geom::point_3f position, geom::point_3f blade_origin, geom::vector_2f const & texcoord)
		{
			vertex v;

			position -= geom::vector_3f{0.5f, 2.f, 0.5f};

			blade_origin -= geom::vector_3f{0.5f, 2.f, 0.5f};

			v.position[0] = char(position[0] * 128.f);
			v.position[1] = char(position[1] * 64.f);
			v.position[2] = char(position[2] * 128.f);

			v.blade_origin[0] = char(blade_origin[0] * 128.f);
			v.blade_origin[1] = char(blade_origin[1] * 64.f);
			v.blade_origin[2] = char(blade_origin[2] * 128.f);

			v.texcoord[0] = (unsigned char)(texcoord[0] * 255.f);
			v.texcoord[1] = (unsigned char)(texcoord[1] * 255.f);

			vertices.push_back(v);
		};

		auto push_blade = [&](geom::point_3f const & origin, geom::vector_2f const & direction, float height, float width, float depth)
		{
			unsigned int const base_index = vertices.size();

			int const n = 4;

			auto const normal = geom::ort(direction);

			auto at = [&](float t, float s)
			{
				s -= 0.5f;
				s *= (1.f - t);

				return geom::point_3f
				{
					origin[0] + direction[0] * t * depth + normal[0] * s * width,
					origin[1] + height * std::sqrt(t),
					origin[2] + direction[1] * t * depth + normal[1] * s * width,
				};
			};

			geom::vector_2f texcoord0, texcoord1;

			texcoord0[0] = std::uniform_real_distribution<float>{}(rng);
			texcoord0[1] = std::uniform_real_distribution<float>{0.f, 0.5f}(rng);

			texcoord1[0] = std::uniform_real_distribution<float>{}(rng);
			texcoord1[1] = std::uniform_real_distribution<float>{0.5f, 1.f}(rng);

			for (int i = 0; i <= n; ++i)
			{
				float const t = float(i) / n;

				geom::point_3f const p0 = at(t, 0.f);
				geom::point_3f const p1 = at(t, 1.f);

				auto texcoord = geom::lerp(texcoord0, texcoord1, t);

				push_vertex(p0, origin, texcoord);
				push_vertex(p1, origin, texcoord);
			}

			for (unsigned int i = 0; i < n; ++i)
			{
				indices.push_back(base_index + 2 * i + 0);
				indices.push_back(base_index + 2 * i + 1);
				indices.push_back(base_index + 2 * i + 3);

				indices.push_back(base_index + 2 * i + 0);
				indices.push_back(base_index + 2 * i + 3);
				indices.push_back(base_index + 2 * i + 2);
			}
		};

		for (int i = 0; i < 256; ++i)
		{
			float const ox = std::uniform_real_distribution<float>{}(rng);
			float const oz = std::uniform_real_distribution<float>{}(rng);

			float const height = std::uniform_real_distribution<float>{0.4f, 1.f}(rng);

			float const width = std::uniform_real_distribution<float>{0.0625f, 0.125f}(rng);
			float const depth = std::uniform_real_distribution<float>{0.1f, 0.5f}(rng);

			auto const direction = geom::random::uniform_sphere_vector_distribution<geom::vector_2f>{1.f}(rng);

			push_blade(geom::point_3f{ox, 0.f, oz}, direction, height, width, depth);
		}

		indices_count = indices.size();


		for (std::size_t z = 0; z < map.dimensions[2]; ++z)
		{
			for (std::size_t x = 0; x < map.dimensions[0]; ++x)
			{
				std::size_t y;
				for (y = 0; y < map.dimensions[1] && !transparent(map(x, y, z)); ++y);

				instance v;
				v.origin = { short(x), short(y), short(z) };
				v.rotation = std::uniform_int_distribution<unsigned char>{0, 7}(rng);
				v.size = std::uniform_int_distribution<unsigned char>{127, 191}(rng);
				instances.push_back(v);
			}
		}

		instances_count = instances.size();

		vao.bind_scoped()
			.with([&](auto & binder){
				index_vbo.bind(gl::ELEMENT_ARRAY_BUFFER)
					.data(indices);

				vbo.bind_scoped(gl::ARRAY_BUFFER)
					.data(vertices)
					.with([&]{
						binder
							.attrib(0, std::get<0>(attribs))
							.attrib(1, std::get<2>(attribs))
							.attrib(4, std::get<1>(attribs))
							;
					});

				instance_vbo.bind_scoped(gl::ARRAY_BUFFER)
					.data(instances)
					.with([&]{
						binder
							.attrib(2, std::get<0>(instance_attribs))
							.divisor(2, 1)
							.attrib_i(3, std::get<1>(instance_attribs))
							.divisor(3, 1)
							.attrib(5, std::get<2>(instance_attribs))
							.divisor(5, 1)
							;
					});
			});
	}

	grass::grass (block_map const & map)
		: pimpl_(std::make_unique<implementation>(map))
	{ }

	grass::~grass ( )
	{ }

	void grass::render (geom::matrix_4x4f const & transform)
	{
		gl::Enable(gl::DEPTH_TEST);

		impl().texture.bind_scoped(gl::TEXTURE_2D).with([&]{
			impl().program.bind_scoped()
				.uniform("u_transform", transform)
				.uniform("u_texture", 0)
				.with([&]{
					impl().vao.bind_scoped()
						.draw_elements_instanced<unsigned int>(gl::TRIANGLES, impl().indices_count, 0, impl().instances_count);
				});
		});

		gl::Disable(gl::DEPTH_TEST);
	}

}
