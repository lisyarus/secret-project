#include <secret/world.hpp>
#include <secret/terrain.hpp>
#include <secret/light.hpp>

namespace secret
{

	struct world::implementation
	{
		geom::vector_3ul const size;
		secret::terrain terrain;
		secret::light light;

		implementation (geom::vector_3ul const & size, world_generator const & generator);
	};

	static std::size_t round_up (std::size_t x, std::size_t c)
	{
		return x + ((x % c) == 0 ? 0 : c - (x % c));
	}

	static geom::vector_3ul round_up (geom::vector_3ul x, std::size_t c)
	{
		return {
			round_up(x[0], c),
			round_up(x[1], c),
			round_up(x[2], c)
		};
	}

	world::implementation::implementation (geom::vector_3ul const & size, world_generator const & generator)
		: size(round_up(size, chunk_size))
		, terrain(generator.generate_terrain(this->size))
		, light(terrain.blocks())
	{ }

	world::world (geom::vector_3ul const & size, world_generator const & generator)
		: pimpl_(std::make_unique<implementation>(size, generator))
	{ }

	world::~world ( )
	{ }

	geom::vector_3ul world::size ( ) const
	{
		return impl().size;
	}

	void world::step (std::size_t tick_count, float dt)
	{
		impl().terrain.step(tick_count);
	}

	void world::render (world_render_options const & options)
	{
		impl().light.update(impl().terrain.blocks());
		impl().terrain.render(options, impl().light);
	}

	world::raycast_result world::raycast (geom::ray_3f const & ray) const
	{
		if (auto terrain_raycast = impl().terrain.raycast(ray))
		{
			return terrain_hit{ terrain_raycast->block, terrain_raycast->face };
		}

		return no_hit{};
	}

	void world::set_block (geom::vector_3ul const & position, block b)
	{
		impl().terrain.set(position[0], position[1], position[2], b);

		if (transparent(b))
			impl().light.set_transparent(position);
		else
			impl().light.set_opaque(position);
	}

}
