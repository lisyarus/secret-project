#include <secret/world_generator.hpp>

#include <secret/perlin.hpp>

#include <secret/terrain.hpp>

namespace secret
{

	struct default_world_generator
		: world_generator
	{
		block_map generate_terrain (geom::vector_3ul const & size) const override;
	};

	block_map default_world_generator::generate_terrain (geom::vector_3ul const & size) const
	{
		block_map result(size);

		std::default_random_engine rng{std::random_device{}()};

		static constexpr std::size_t octaves = 2;

		std::size_t const max_block_size = std::max(size[0], size[1]) / 4;

		geom::ndarray<float, 2> perlin_maps[octaves];

		for (std::size_t o = 0; o < octaves; ++o)
		{
			std::size_t const block_size = max_block_size >> (2*o);
			perlin_maps[o] = perlin(rng, {size[0] / block_size, size[2] / block_size}, block_size, false);
		}

		for (std::size_t z = 0; z < size[2]; ++z)
		{
			for (std::size_t y = 0; y < size[1]; ++y)
			{
				for (std::size_t x = 0; x < size[0]; ++x)
				{
					float max_y_f = size[1] / 2;

					for (std::size_t o = 0; o < octaves; ++o)
					{
						max_y_f += perlin_maps[o](x, z) * 48.f * std::pow(2.f, -float(2*o));
					}

					std::size_t max_y = static_cast<std::size_t>(max_y_f);

					if (y < max_y)
						result(x, y, z) = block::stone;
					else if (y == max_y)
						result(x, y, z) = block::dirt;
					else
						result(x, y, z) = block::air;
				}
			}
		}

		return result;
	}

	std::unique_ptr<world_generator> make_default_world_generator ( )
	{
		return std::make_unique<default_world_generator>();
	}

}
