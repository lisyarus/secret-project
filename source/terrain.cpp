#include <secret/terrain.hpp>

#include <secret/block.hpp>
#include <secret/block_texture.hpp>
#include <secret/light.hpp>

#include <glow/object/buffer.hpp>
#include <glow/object/vertex_array.hpp>
#include <glow/object/program.hpp>
#include <glow/object/texture.hpp>

#include <geom/transform/homogeneous.hpp>
#include <geom/alias/interval.hpp>
#include <geom/operations/point.hpp>
#include <geom/operations/rectangle.hpp>
#include <geom/operations/compare/vector.hpp>
#include <geom/io/vector.hpp>

#include <util/profiler.hpp>
#include <util/to_string.hpp>

#include <set>
#include <random>
#include <queue>

namespace secret
{

	namespace
	{

		template <typename Cell, typename Callback>
		void optimize_chunk_plane (Cell (&mask)[chunk_size][chunk_size], Callback && callback)
		{
			for (std::size_t start_j = 0; start_j < chunk_size; ++start_j)
			{
				for (std::size_t start_i = 0; start_i < chunk_size; ++start_i)
				{
					auto const c = mask[start_i][start_j];

					if (c == Cell(0))
						continue;

					std::size_t end_i;
					for (end_i = start_i + 1; end_i < chunk_size; ++end_i)
						if (mask[end_i][start_j] != c)
							break;

					std::size_t end_j;
					for (end_j = start_j + 1; end_j < chunk_size; ++end_j)
					{
						bool all = true;

						for (std::size_t i = start_i; i < end_i; ++i)
						{
							if (mask[i][end_j] != c)
							{
								all = false;
								break;
							}
						}

						if (!all)
							break;
					}

					for (std::size_t i = start_i; i < end_i; ++i)
						for (std::size_t j = start_j; j < end_j; ++j)
							mask[i][j] = Cell(0);

					callback(c, geom::rectangle_2ul{{ {start_i, end_i}, {start_j, end_j} }});

					start_i = end_i - 1;
				}
			}
		}

		struct chunk_render_data
		{
			glow::buffer vertex_vbo;
			glow::vertex_array vao;

			struct face_render_data
			{
				int index_start;
				int index_count;
			};

			face_render_data faces[6];

			geom::rectangle_3f bbox;

			chunk_render_data ( );

			void setup (glow::buffer & index_vbo);

			void build (block_map const & blocks, geom::vector_3ul const & ch);

			bool empty ( ) const;

			void release ( );
		};

		struct terrain_vertex
		{
			geom::point_2s position_xz;
			geom::vector_2ub texcoord;
			std::uint8_t position_y;
			std::uint8_t texture_layer;
		};

		chunk_render_data::chunk_render_data ( )
		{
			for (std::size_t f = 0; f < 6; ++f)
			{
				faces[f] = { 0, 0 };
			}
		}

		void chunk_render_data::setup (glow::buffer & index_vbo)
		{
			constexpr auto attribs = glow::interleaved<geom::point_2s, geom::vector_2ub, geom::point_1ub, geom::vector_1ub>();

			vertex_vbo.bind_scoped(gl::ARRAY_BUFFER)
				.with([&]{
					vao.bind_scoped()
						.attrib(0, std::get<0>(attribs))
						.attrib(1, std::get<2>(attribs))
						.attrib(2, std::get<1>(attribs))
						.attrib(3, std::get<3>(attribs))
						.with([&]{
							index_vbo.bind(gl::ELEMENT_ARRAY_BUFFER);
						});
				});
		}

		void chunk_render_data::build (block_map const & blocks, geom::vector_3ul const & ch)
		{
			geom::vector_3ul const cho = ch * chunk_size;

			geom::point_3s const chunk_origin = geom::point_3s::zero() + geom::cast<short>(cho);
			geom::point_3f const chunk_origin_f = geom::cast<float>(chunk_origin);

			bbox = {{
				{ 0.f, float(chunk_size) },
				{ 0.f, float(chunk_size) },
				{ 0.f, float(chunk_size) },
			}};

			bbox += (chunk_origin_f - geom::point_3f::zero());

			static_assert(sizeof(terrain_vertex) == 8);

			std::vector<terrain_vertex> vertices;
			vertices.reserve(chunk_size * chunk_size * chunk_size * 6 * 4 * 6 / 2);

			auto push_vertex = [&](int ix, int iy, int iz, int iu, int iv, std::uint8_t t)
			{
				terrain_vertex v;
				auto pos = chunk_origin + geom::vector_3s{ short(ix), short(iy), short(iz) };
				v.position_xz = { pos[0], pos[2] };
				v.position_y = static_cast<std::uint8_t>(pos[1]);
				v.texcoord = geom::vector_2ub{ (unsigned char)(iu), (unsigned char)(iv) };
				v.texture_layer = t;
				vertices.push_back(v);
			};

			block mask[chunk_size][chunk_size];

			// -x

			faces[0].index_start = 3 * vertices.size() / 2;

			if (cho[0] == 0)
			{
				for (std::size_t z = 0; z < chunk_size; ++z)
				{
					std::size_t const rz = cho[2] + z;

					for (std::size_t y = 0; y < chunk_size; ++y)
					{
						std::size_t const ry = cho[1] + y;

						block const b = blocks(0, ry, rz);

						if (transparent(b))
							mask[y][z] = block::air;
						else if (false
							|| (ry > 0 && transparent(blocks(0, ry-1, rz)))
							|| (ry+1 < blocks.dimensions[1] && transparent(blocks(0, ry+1, rz)))
							|| (rz > 0 && transparent(blocks(0, ry, rz-1)))
							|| (rz+1 < blocks.dimensions[2] && transparent(blocks(0, ry, rz+1)))
							)
							mask[y][z] = b;
						else
							mask[y][z] = b;//block::terra_incognita;
					}
				}

				optimize_chunk_plane(mask, [&](block b, geom::rectangle_2ul const & r)
				{
					auto const t = texture_layer(b);

					auto const u = r[1].length();
					auto const v = r[0].length();

					push_vertex(0, r[0].inf, r[1].inf, 0, 0, t);
					push_vertex(0, r[0].inf, r[1].sup, u, 0, t);
					push_vertex(0, r[0].sup, r[1].inf, 0, v, t);
					push_vertex(0, r[0].sup, r[1].sup, u, v, t);
				});
			}

			for (std::size_t x = 1; x <= chunk_size; ++x)
			{
				std::size_t const rx = cho[0] + x;

				if (rx == blocks.dimensions[0])
					continue;

				for (std::size_t z = 0; z < chunk_size; ++z)
				{
					std::size_t const rz = cho[2] + z;

					for (std::size_t y = 0; y < chunk_size; ++y)
					{
						std::size_t const ry = cho[1] + y;

						block const b = blocks(rx, ry, rz);

						if (transparent(b))
							mask[y][z] = block::air;
						else if (transparent(blocks(rx-1, ry, rz)))
							mask[y][z] = b;
						else
							mask[y][z] = block::air;
					}
				}

				optimize_chunk_plane(mask, [&](block b, geom::rectangle_2ul const & r)
				{
					auto const t = texture_layer(b);

					auto const u = r[1].length();
					auto const v = r[0].length();

					push_vertex(x, r[0].inf, r[1].inf, 0, 0, t);
					push_vertex(x, r[0].inf, r[1].sup, u, 0, t);
					push_vertex(x, r[0].sup, r[1].inf, 0, v, t);
					push_vertex(x, r[0].sup, r[1].sup, u, v, t);
				});
			}

			faces[0].index_count = 3 * vertices.size() / 2 - faces[0].index_start;

			// +x

			faces[1].index_start = 3 * vertices.size() / 2;

			for (std::size_t x = 0; x < chunk_size; ++x)
			{
				std::size_t const rx = cho[0] + x;

				if (rx == 0)
					continue;

				for (std::size_t z = 0; z < chunk_size; ++z)
				{
					std::size_t const rz = cho[2] + z;

					for (std::size_t y = 0; y < chunk_size; ++y)
					{
						std::size_t const ry = cho[1] + y;

						block const b = blocks(rx-1, ry, rz);

						if (transparent(b))
							mask[y][z] = block::air;
						else if (transparent(blocks(rx, ry, rz)))
							mask[y][z] = b;
						else
							mask[y][z] = block::air;
					}
				}

				optimize_chunk_plane(mask, [&](block b, geom::rectangle_2ul const & r)
				{
					auto const t = texture_layer(b);

					auto const u = r[1].length();
					auto const v = r[0].length();

					push_vertex(x, r[0].inf, r[1].inf, 0, 0, t);
					push_vertex(x, r[0].sup, r[1].inf, 0, v, t);
					push_vertex(x, r[0].inf, r[1].sup, u, 0, t);
					push_vertex(x, r[0].sup, r[1].sup, u, v, t);
				});
			}

			faces[1].index_count = 3 * vertices.size() / 2 - faces[1].index_start;

			// -y

			faces[2].index_start = 3 * vertices.size() / 2;

			for (std::size_t y = 1; y <= chunk_size; ++y)
			{
				std::size_t const ry = cho[1] + y;

				if (ry == blocks.dimensions[1])
					continue;

				for (std::size_t z = 0; z < chunk_size; ++z)
				{
					std::size_t const rz = cho[2] + z;

					for (std::size_t x = 0; x < chunk_size; ++x)
					{
						std::size_t const rx = cho[0] + x;

						block const b = blocks(rx, ry, rz);

						if (transparent(b))
							mask[x][z] = block::air;
						else if (transparent(blocks(rx, ry-1, rz)))
							mask[x][z] = b;
						else
							mask[x][z] = block::air;
					}
				}

				optimize_chunk_plane(mask, [&](block b, geom::rectangle_2ul const & r)
				{
					auto const t = texture_layer(b);

					auto const u = r[0].length();
					auto const v = r[1].length();

					push_vertex(r[0].inf, y, r[1].inf, 0, 0, t);
					push_vertex(r[0].sup, y, r[1].inf, u, 0, t);
					push_vertex(r[0].inf, y, r[1].sup, 0, v, t);
					push_vertex(r[0].sup, y, r[1].sup, u, v, t);
				});
			}

			faces[2].index_count = 3 * vertices.size() / 2 - faces[2].index_start;

			// +y

			faces[3].index_start = 3 * vertices.size() / 2;

			for (std::size_t y = 0; y < chunk_size; ++y)
			{
				std::size_t const ry = cho[1] + y;

				if (ry == 0)
					continue;

				for (std::size_t z = 0; z < chunk_size; ++z)
				{
					std::size_t const rz = cho[2] + z;

					for (std::size_t x = 0; x < chunk_size; ++x)
					{
						std::size_t const rx = cho[0] + x;

						block const b = blocks(rx, ry-1, rz);

						if (transparent(b))
							mask[x][z] = block::air;
						else if (transparent(blocks(rx, ry, rz)))
							mask[x][z] = b;
						else
							mask[x][z] = block::air;
					}
				}

				optimize_chunk_plane(mask, [&](block b, geom::rectangle_2ul const & r)
				{
					auto const t = texture_layer(b);

					auto const u = r[0].length();
					auto const v = r[1].length();

					push_vertex(r[0].inf, y, r[1].inf, 0, 0, t);
					push_vertex(r[0].inf, y, r[1].sup, 0, v, t);
					push_vertex(r[0].sup, y, r[1].inf, u, 0, t);
					push_vertex(r[0].sup, y, r[1].sup, u, v, t);
				});
			}

			faces[3].index_count = 3 * vertices.size() / 2 - faces[3].index_start;

			// -z

			faces[4].index_start = 3 * vertices.size() / 2;

			for (std::size_t z = 1; z <= chunk_size; ++z)
			{
				std::size_t const rz = cho[2] + z;

				if (rz == blocks.dimensions[2])
					continue;

				for (std::size_t y = 0; y < chunk_size; ++y)
				{
					std::size_t const ry = cho[1] + y;

					for (std::size_t x = 0; x < chunk_size; ++x)
					{
						std::size_t const rx = cho[0] + x;

						block const b = blocks(rx, ry, rz);

						if (transparent(b))
							mask[x][y] = block::air;
						else if (transparent(blocks(rx, ry, rz-1)))
							mask[x][y] = b;
						else
							mask[x][y] = block::air;
					}
				}

				optimize_chunk_plane(mask, [&](block b, geom::rectangle_2ul const & r)
				{
					auto const t = texture_layer(b);

					auto const u = r[0].length();
					auto const v = r[1].length();

					push_vertex(r[0].inf, r[1].inf, z, 0, 0, t);
					push_vertex(r[0].inf, r[1].sup, z, 0, v, t);
					push_vertex(r[0].sup, r[1].inf, z, u, 0, t);
					push_vertex(r[0].sup, r[1].sup, z, u, v, t);
				});
			}

			faces[4].index_count = 3 * vertices.size() / 2 - faces[4].index_start;

			// +z

			faces[5].index_start = 3 * vertices.size() / 2;

			for (std::size_t z = 0; z < chunk_size; ++z)
			{
				std::size_t const rz = cho[2] + z;

				if (rz == 0)
					continue;

				for (std::size_t y = 0; y < chunk_size; ++y)
				{
					std::size_t const ry = cho[1] + y;

					for (std::size_t x = 0; x < chunk_size; ++x)
					{
						std::size_t const rx = cho[0] + x;

						block const b = blocks(rx, ry, rz-1);

						if (transparent(b))
							mask[x][y] = block(0);
						else if (transparent(blocks(rx, ry, rz)))
							mask[x][y] = b;
						else
							mask[x][y] = block(0);
					}
				}

				optimize_chunk_plane(mask, [&](block b, geom::rectangle_2ul const & r)
				{
					auto const t = texture_layer(b);

					auto const u = r[0].length();
					auto const v = r[1].length();

					push_vertex(r[0].inf, r[1].inf, z, 0, 0, t);
					push_vertex(r[0].sup, r[1].inf, z, u, 0, t);
					push_vertex(r[0].inf, r[1].sup, z, 0, v, t);
					push_vertex(r[0].sup, r[1].sup, z, u, v, t);
				});
			}

			faces[5].index_count = 3 * vertices.size() / 2 - faces[5].index_start;

			vertex_vbo.bind_scoped(gl::ARRAY_BUFFER)
				.data(vertices);
		}

		bool chunk_render_data::empty ( ) const
		{
			return (faces[5].index_start + faces[5].index_count) > 0;
		}

		void chunk_render_data::release ( )
		{
			if (!empty())
			{
				vertex_vbo.bind_scoped(gl::ARRAY_BUFFER).data(nullptr, 0);

				for (std::size_t f = 0; f < 6; ++f)
				{
					faces[f].index_start = 0;
					faces[f].index_count = 0;
				}
			}
		}

		glow::buffer make_chunk_index_vbo ( )
		{
			std::size_t const max_index_count = 12 * chunk_size * chunk_size * chunk_size;

			std::vector<std::uint32_t> indices(max_index_count);

			for (std::size_t i = 0, j = 0; i < max_index_count; i += 6, j += 4)
			{
				indices[i + 0] = j + 0;
				indices[i + 1] = j + 1;
				indices[i + 2] = j + 2;
				indices[i + 3] = j + 2;
				indices[i + 4] = j + 1;
				indices[i + 5] = j + 3;
			}

			glow::buffer vbo;
			vbo.bind_scoped(gl::ARRAY_BUFFER).data(indices);
			return vbo;
		}

		const char block_program_vertex_source[] =
		R"(#version 330

		uniform mat4 u_modelview;
		uniform mat4 u_projection;
		uniform float u_block_texture_size;
		uniform vec3 u_normal;

		layout (location = 0) in vec2 in_position_xz;
		layout (location = 1) in float in_position_y;
		layout (location = 2) in vec2 in_texcoord;
		layout (location = 3) in float in_texture_layer;

		out vec3 texcoord;
		out vec3 position;

		void main ( )
		{
			position = vec3(in_position_xz.x, in_position_y, in_position_xz.y);

			gl_Position = u_projection * u_modelview * vec4(position, 1.0);

			texcoord = vec3(in_texcoord, in_texture_layer);
		}

		)";

		const char block_program_fragment_source[] =
		R"(#version 330

		uniform sampler2DArray u_texture;
		uniform sampler3D u_light_texture;
		uniform vec3 u_normal;
		uniform vec4 u_sunlight_color;
		uniform vec3 u_world_size;

		in vec3 texcoord;
		in vec3 position;

		layout (location = 0) out vec4 out_color;
		layout (location = 1) out vec3 out_normal;
		layout (location = 2) out vec3 out_position;

		void main ( )
		{
			vec3 p = fract((position + u_normal * 0.5) / 32.0) - u_normal / 64.0;
			vec3 lightmap_texcoord = (p * 32.0 + vec3(1.0, 1.0, 1.0) + u_normal * 0.0) / 34.0;

			vec2 light = texture(u_light_texture, lightmap_texcoord).rg;
			light += vec2(1.0 / 16.0, 1.0 / 16.0);

			light.g *= mix(0.5, 1.0, (1.0 + dot(u_normal, vec3(0.0, 0.8, 0.6))) / 2.0);

			out_color = texture(u_texture, texcoord) *
				vec4(vec3(1.0, 0.75, 0.25) * light.r + u_sunlight_color.rgb * light.g, 1.0);

			out_normal = u_normal * 0.5 + vec3(0.5, 0.5, 0.5);
			out_position = position / u_world_size;
		}

		)";

	}

	struct terrain::implementation
	{
		glow::buffer chunk_index_vbo;
		glow::texture block_texture;
		glow::program block_program;
		geom::ndarray<chunk_render_data, 3> chunks;
		geom::ndarray<bool, 3> chunks_need_update;
		std::set<geom::vector_3ul> blocks_need_update;

		implementation (geom::vector_3ul world_size, bool chunks_initial_update);

		void set_moore_neighbours_need_update (geom::vector_3ul const & p);
		void set_von_neumann_neighbours_need_update (geom::vector_3ul const & p);
	};

	terrain::implementation::implementation (geom::vector_3ul world_size, bool chunks_initial_update)
		: chunk_index_vbo(make_chunk_index_vbo())
		, block_texture(make_block_texture())
		, block_program(
			glow::vertex_shader(block_program_vertex_source),
			glow::fragment_shader(block_program_fragment_source))
		, chunks(world_size)
		, chunks_need_update(world_size, chunks_initial_update)
	{
		for (std::size_t cz = 0; cz < world_size[2]; ++cz)
		{
			for (std::size_t cy = 0; cy < world_size[1]; ++cy)
			{
				for (std::size_t cx = 0; cx < world_size[0]; ++cx)
				{
					chunks(cx, cy, cz).setup(chunk_index_vbo);
				}
			}
		}
	}

	void terrain::implementation::set_moore_neighbours_need_update (geom::vector_3ul const & p)
	{
		auto const ch = p / chunk_size;

		chunks_need_update(ch) = true;

		if (ch[0] > 0 && (p[0] % chunk_size == 0))
			chunks_need_update(ch[0] - 1, ch[1], ch[2]) = true;

		if (ch[0]+1 < chunks.dimensions[0] && ((p[0]+1) % chunk_size == 0))
			chunks_need_update(ch[0] + 1, ch[1], ch[2]) = true;

		if (ch[1] > 0 && (p[1] % chunk_size == 0))
			chunks_need_update(ch[0], ch[1] - 1, ch[2]) = true;

		if (ch[1]+1 < chunks.dimensions[1] && ((p[1]+1) % chunk_size == 0))
			chunks_need_update(ch[0], ch[1] + 1, ch[2]) = true;

		if (ch[2] > 0 && (p[2] % chunk_size == 0))
			chunks_need_update(ch[0], ch[1], ch[2] - 1) = true;

		if (ch[2]+1 < chunks.dimensions[2] && ((p[2]+1) % chunk_size == 0))
			chunks_need_update(ch[0], ch[1], ch[2] + 1) = true;
	}

	void terrain::implementation::set_von_neumann_neighbours_need_update (geom::vector_3ul const & p)
	{
		auto const ch = p / chunk_size;

		auto border = [](std::size_t x, int t)
		{
			switch (t)
			{
			case -1: return (x % chunk_size) == 0;
			case 0: return true;
			case 1: return ((x + 1) % chunk_size) == 0;
			}
		};

		for (int tx : {-1, 0, 1})
		{
			for (int ty : {-1, 0, 1})
			{
				for (int tz : {-1, 0, 1})
				{
					std::size_t const cx = ch[0] + tx;
					std::size_t const cy = ch[1] + ty;
					std::size_t const cz = ch[2] + tz;

					if (true
						&& cx < chunks.dimensions[0]
						&& cy < chunks.dimensions[1]
						&& cz < chunks.dimensions[2]
						&& border(p[0], tx)
						&& border(p[1], ty)
						&& border(p[2], tz)
						)
						chunks_need_update(cx, cy, cz) = true;
				}
			}
		}
	}

	terrain::terrain (block_map blocks)
		: blocks_(std::move(blocks))
		, pimpl_(std::make_unique<implementation>(blocks_.dimensions / chunk_size, true))
	{
		if (false
			|| blocks_.dimensions[0] % chunk_size != 0
			|| blocks_.dimensions[1] % chunk_size != 0
			|| blocks_.dimensions[2] % chunk_size != 0)
		{
			throw std::runtime_error("world size should be proportional to chunk_size");
		}
	}

	terrain::~terrain ( )
	{ }

	void terrain::set (std::size_t x, std::size_t y, std::size_t z, block b)
	{
		blocks_(x, y, z) = b;

		geom::vector_3ul const c { x, y, z };

		if (b == block::sand)
			impl().blocks_need_update.insert(c);
		else if (b == block::air && y < blocks_.dimensions[1] && blocks_(x, y+1, z) == block::sand)
			impl().blocks_need_update.insert({x, y+1, z});
		else if (b == block::air && y < blocks_.dimensions[1] && x > 0 && blocks_(x-1, y+1, z) == block::sand && blocks_(x, y+1, z) == block::air)
			impl().blocks_need_update.insert({x-1, y+1, z});
		else if (b == block::air && y > 0 && x > 0 && blocks_(x-1, y, z) == block::sand && blocks_(x, y-1, z) == block::air)
			impl().blocks_need_update.insert({x-1, y, z});
		else if (b == block::air && y < blocks_.dimensions[1] && x+1 < blocks_.dimensions[0] && blocks_(x+1, y+1, z) == block::sand && blocks_(x, y+1, z) == block::air)
			impl().blocks_need_update.insert({x+1, y+1, z});
		else if (b == block::air && y > 0 && x+1 < blocks_.dimensions[0] && blocks_(x+1, y, z) == block::sand && blocks_(x, y-1, z) == block::air)
			impl().blocks_need_update.insert({x+1, y, z});
		else if (b == block::air && y < blocks_.dimensions[1] && z > 0 && blocks_(x, y+1, z-1) == block::sand && blocks_(x, y+1, z) == block::air)
			impl().blocks_need_update.insert({x, y+1, z-1});
		else if (b == block::air && y > 0 && z > 0 && blocks_(x, y, z-1) == block::sand && blocks_(x, y-1, z) == block::air)
			impl().blocks_need_update.insert({x, y, z-1});
		else if (b == block::air && y < blocks_.dimensions[1] && z+1 < blocks_.dimensions[2] && blocks_(x, y+1, z+1) == block::sand && blocks_(x, y+1, z) == block::air)
			impl().blocks_need_update.insert({x, y+1, z+1});
		else if (b == block::air && y > 0 && z+1 < blocks_.dimensions[2] && blocks_(x, y, z+1) == block::sand && blocks_(x, y-1, z) == block::air)
			impl().blocks_need_update.insert({x, y, z+1});

		impl().set_moore_neighbours_need_update(c);
	}

	std::optional<terrain::raycast_result> terrain::raycast (geom::ray_3f const & ray) const
	{
		geom::rectangle_3s bbox
		{{
			{ 0, short(blocks_.dimensions[0]) },
			{ 0, short(blocks_.dimensions[1]) },
			{ 0, short(blocks_.dimensions[2]) },
		}};

		geom::point_3i current_block;

		std::uint8_t face = 3;

		if (geom::contains(geom::cast<float>(bbox), ray.origin))
		{
			current_block =
			{
				int(std::floor(ray.origin[0])),
				int(std::floor(ray.origin[1])),
				int(std::floor(ray.origin[2])),
			};

			// face intetionally left unspecified :(
		}
		else
		{
			float p_in[3];
			float p_out[3];

			p_in[0] = (ray.direction[0] > 0.f) ? bbox[0].inf : bbox[0].sup;
			p_in[1] = (ray.direction[1] > 0.f) ? bbox[1].inf : bbox[1].sup;
			p_in[2] = (ray.direction[2] > 0.f) ? bbox[2].inf : bbox[2].sup;

			p_out[0] = (ray.direction[0] < 0.f) ? bbox[0].inf : bbox[0].sup;
			p_out[1] = (ray.direction[1] < 0.f) ? bbox[1].inf : bbox[1].sup;
			p_out[2] = (ray.direction[2] < 0.f) ? bbox[2].inf : bbox[2].sup;

			float t_in[3];
			float t_out[3];

			t_in[0] = (p_in[0] - ray.origin[0]) / ray.direction[0];
			t_in[1] = (p_in[1] - ray.origin[1]) / ray.direction[1];
			t_in[2] = (p_in[2] - ray.origin[2]) / ray.direction[2];

			t_out[0] = (p_out[0] - ray.origin[0]) / ray.direction[0];
			t_out[1] = (p_out[1] - ray.origin[1]) / ray.direction[1];
			t_out[2] = (p_out[2] - ray.origin[2]) / ray.direction[2];

			float t = std::max({t_in[0], t_in[1], t_in[2]});

			if (t < std::min({t_out[0], t_out[1], t_out[2]}))
			{
				auto const p = ray.origin + t * ray.direction;

				if (t_in[0] > t_in[1] && t_in[0] > t_in[2])
				{
					// x

					current_block[1] = std::floor(p[1]);
					current_block[2] = std::floor(p[2]);

					if (ray.direction[0] > 0.f)
					{
						current_block[0] = 0;
						face = 0;
					}
					else
					{
						current_block[0] = blocks_.dimensions[0] - 1;
						face = 1;
					}
				}
				else if (t_in[1] > t_in[2])
				{
					// y

					current_block[0] = std::floor(p[0]);
					current_block[2] = std::floor(p[2]);

					if (ray.direction[1] > 0.f)
					{
						current_block[1] = 0;
						face = 2;
					}
					else
					{
						current_block[1] = blocks_.dimensions[1] - 1;
						face = 3;
					}
				}
				else
				{
					// z

					current_block[0] = std::floor(p[0]);
					current_block[1] = std::floor(p[1]);

					if (ray.direction[2] > 0.f)
					{
						current_block[2] = 0;
						face = 4;
					}
					else
					{
						current_block[2] = blocks_.dimensions[2] - 1;
						face = 5;
					}
				}
			}
			else
				return std::nullopt;
		}

		auto to_block_index = [this](geom::point_3i p)
		{
			return geom::cast<std::size_t>(p) - geom::point_3ul::zero();
		};

		while (geom::contains(geom::cast<float>(bbox), geom::cast<float>(current_block) + geom::vector_3f{0.5f, 0.5f, 0.5f})
			&& transparent(blocks_(to_block_index(current_block))))
		{
			float p[3];

			p[0] = (ray.direction[0] > 0.f) ? current_block[0] + 1.f : current_block[0];
			p[1] = (ray.direction[1] > 0.f) ? current_block[1] + 1.f : current_block[1];
			p[2] = (ray.direction[2] > 0.f) ? current_block[2] + 1.f : current_block[2];

			float t[3];

			t[0] = (p[0] - ray.origin[0]) / ray.direction[0];
			t[1] = (p[1] - ray.origin[1]) / ray.direction[1];
			t[2] = (p[2] - ray.origin[2]) / ray.direction[2];

			if (t[0] < t[1] && t[0] < t[2])
			{
				// x
				if (ray.direction[0] > 0.f)
				{
					current_block[0] += 1;
					face = 0;
				}
				else
				{
					current_block[0] -= 1;
					face = 1;
				}
			}
			else if (t[1] < t[2])
			{
				// y
				if (ray.direction[1] > 0.f)
				{
					current_block[1] += 1;
					face = 2;
				}
				else
				{
					current_block[1] -= 1;
					face = 3;
				}
			}
			else
			{
				// z
				if (ray.direction[2] > 0.f)
				{
					current_block[2] += 1;
					face = 4;
				}
				else
				{
					current_block[2] -= 1;
					face = 5;
				}
			}
		}

		if (geom::contains(geom::cast<float>(bbox), geom::cast<float>(current_block) + geom::vector_3f{0.5f, 0.5f, 0.5f}))
			return raycast_result{ to_block_index(current_block), face };
		else
			return std::nullopt;
	}

	void terrain::render (world_render_options const & options, secret::light const & light)
	{
		auto const & modelview = options.modelview;
		auto const & projection = options.projection;
		auto const & camera_pos = options.camera_pos;
		auto const & camera_target = options.camera_target;
		auto const & clip_planes = options.camera_clip_planes;

		std::vector<geom::vector_3ul> chunks_needing_update;

		std::size_t total_vertex_count = 0;

		for (std::size_t cz = 0; cz < impl().chunks.dimensions[2]; ++cz)
		{
			for (std::size_t cy = 0; cy < impl().chunks.dimensions[1]; ++cy)
			{
				for (std::size_t cx = 0; cx < impl().chunks.dimensions[0]; ++cx)
				{
					if (impl().chunks_need_update(cx, cy, cz))
					{
						chunks_needing_update.push_back({cx, cy, cz});
					}

					auto const & ch = impl().chunks(cx, cy, cz);

					total_vertex_count += ((ch.faces[5].index_start + ch.faces[5].index_count) * 2) / 3;
				}
			}
		}

//		std::cout << "Terrain vertices: " << total_vertex_count << " (" << (total_vertex_count * 8.f / 1024.f / 1024.f) << "MB)" << std::endl;

		auto chunk_center_distance_to_camera = [&](geom::vector_3ul const & ch)
		{
			auto chunk_center = geom::point_3f::zero()
				+ (geom::cast<float>(ch) + geom::vector_3f{0.5f, 0.5f, 0.5f}) * static_cast<float>(chunk_size);
			return geom::distance_sqr(chunk_center, camera_target);
		};

		auto chunks_comparator = [&](geom::vector_3ul const & ch1, geom::vector_3ul const & ch2)
		{
			return chunk_center_distance_to_camera(ch1) < chunk_center_distance_to_camera(ch2);
		};

		std::size_t const max_chunks_update_per_frame = std::min(8ul, chunks_needing_update.size());

		std::partial_sort(
			chunks_needing_update.begin(), chunks_needing_update.begin() + max_chunks_update_per_frame,
			chunks_needing_update.end(), chunks_comparator);


		for (std::size_t i = 0; i < max_chunks_update_per_frame; ++i)
		{
			auto ch = chunks_needing_update[i];

			impl().chunks(ch).build(blocks_, ch);
			impl().chunks_need_update(ch) = false;
		}

		gl::Enable(gl::DEPTH_TEST);
		gl::Enable(gl::CULL_FACE);

		gl::ActiveTexture(gl::TEXTURE0);
		impl().block_texture.bind_scoped(gl::TEXTURE_2D_ARRAY)
			.with([&]{

				float const time = 0.f;

				float const c = std::max(std::cos(time), 0.f);

				geom::vector_4f sunlight_color =
				{
					0.0f + 0.8f * c,
					0.0f + 0.8f * c,
					0.1f + 0.7f * c,
					1.f
				};

				impl().block_program.bind_scoped()
					.uniform("u_modelview", modelview)
					.uniform("u_projection", projection)
					.uniform("u_texture", 0)
					.uniform("u_light_texture", 1)
					.uniform("u_sunlight_color", sunlight_color)
					.uniform("u_world_size", geom::cast<float>(blocks_.dimensions))
					.with([&](auto & binder){

						gl::ActiveTexture(gl::TEXTURE1);

						std::vector<geom::vector_3ul> visible_chunks;

						for (std::size_t cz = 0; cz < impl().chunks.dimensions[2]; ++cz)
						{
							for (std::size_t cy = 0; cy < impl().chunks.dimensions[1]; ++cy)
							{
								for (std::size_t cx = 0; cx < impl().chunks.dimensions[0]; ++cx)
								{
									auto & rd = impl().chunks(cx, cy, cz);

									// Frustum culling

									bool bbox_outside = false;

									for (auto const & plane : clip_planes)
									{
										bool outside = true;
										for (float tx : {0.f, 1.f})
										{
											for (float ty : {0.f, 1.f})
											{
												for (float tz : {0.f, 1.f})
												{
													if (plane * geom::homogeneous(rd.bbox(tx, ty, tz)) > 0.f)
													{
														outside = false;
														break;
													}
												}
											}
										}

										if (outside)
										{
											bbox_outside = true;
											break;
										}
									}

									if (bbox_outside)
										continue;

									visible_chunks.push_back({cx, cy, cz});
								}
							}
						}

						// Face culling
						for (std::size_t f = 0; f < 6; ++f)
						{
							binder.uniform("u_normal", face_normal(f));

							for (auto const & ch : visible_chunks)
							{
								auto & rd = impl().chunks(ch);

								auto & face = rd.faces[f];

								if (face.index_count == 0) continue;

								switch (f)
								{
								case 0:
									if (camera_pos[0] > rd.bbox[0].sup)
										continue;
									break;
								case 1:
									if (camera_pos[0] < rd.bbox[0].inf)
										continue;
									break;
								case 2:
									if (camera_pos[1] > rd.bbox[1].sup)
										continue;
									break;
								case 3:
									if (camera_pos[1] < rd.bbox[1].inf)
										continue;
									break;
								case 4:
									if (camera_pos[2] > rd.bbox[2].sup)
										continue;
									break;
								case 5:
									if (camera_pos[2] < rd.bbox[2].inf)
										continue;
									break;
								}

								light.get_light_texture(ch).bind(gl::TEXTURE_3D);
								rd.vao.bind().template draw_elements<unsigned int>(gl::TRIANGLES, face.index_count, face.index_start);
							}
						}

						glow::vertex_array::null().bind();
						glow::texture::null().bind(gl::TEXTURE_3D);

						gl::ActiveTexture(gl::TEXTURE0);
					});
			});

		gl::Disable(gl::CULL_FACE);
		gl::Disable(gl::DEPTH_TEST);
	}

	static std::array<std::size_t, 4> shuffles4[24] =
	{
		{0, 1, 2, 3},
		{0, 1, 3, 2},
		{0, 2, 1, 3},
		{0, 2, 3, 1},
		{0, 3, 1, 2},
		{0, 3, 2, 1},

		{1, 0, 2, 3},
		{1, 0, 3, 2},
		{1, 2, 0, 3},
		{1, 2, 3, 0},
		{1, 3, 0, 2},
		{1, 3, 2, 0},

		{2, 0, 1, 3},
		{2, 0, 3, 1},
		{2, 1, 0, 3},
		{2, 1, 3, 0},
		{2, 3, 0, 1},
		{2, 3, 1, 0},

		{3, 0, 1, 2},
		{3, 0, 2, 1},
		{3, 1, 0, 2},
		{3, 1, 2, 0},
		{3, 2, 0, 1},
		{3, 2, 1, 0},
	};

	void terrain::step (std::size_t tick_count)
	{
		std::vector<geom::vector_3ul> need_update(impl().blocks_need_update.begin(), impl().blocks_need_update.end());
		impl().blocks_need_update.clear();

		// Sort by descending y first
		std::sort(need_update.begin(), need_update.end(), [](auto const & v1, auto const & v2){
			return std::tie(v1[1], v1[0], v1[2]) > std::tie(v2[1], v2[0], v2[2]);
		});

		for (auto c : need_update)
		{
			if (blocks_(c) == block::sand)
			{
				geom::vector_3ul jump_target_unshuffled[5];

				jump_target_unshuffled[0] = { c[0], c[1]-1, c[2] };
				jump_target_unshuffled[1] = { c[0]-1, c[1]-1, c[2] };
				jump_target_unshuffled[2] = { c[0]+1, c[1]-1, c[2] };
				jump_target_unshuffled[3] = { c[0], c[1]-1, c[2]-1 };
				jump_target_unshuffled[4] = { c[0], c[1]-1, c[2]+1 };

				// A pinch of isotropy
				// A deterministic way to choose one of 24 configurations:
				// 2 per x * 6 per y * 2 per z + time

				std::size_t const permutation = ((c[0] % 2) * 12 + (c[1] % 6) * 2 + (c[2] % 2) + tick_count) % 24;

				geom::vector_3ul jump_target[5];

				jump_target[0] = jump_target_unshuffled[0];

				for (std::size_t i = 0; i < 4; ++i)
					jump_target[i+1] = jump_target_unshuffled[shuffles4[permutation][i]+1];

				for (auto t : jump_target)
				{
					if (t[0] < blocks_.dimensions[0]
						&& t[1] < blocks_.dimensions[1]
						&& t[2] < blocks_.dimensions[2]
						&& blocks_(t) == block::air
						&& ((t[0] == c[0] && t[2] == c[2]) || transparent(blocks_(t[0], c[1], t[2]))))
					{
						set(c[0], c[1], c[2], block::air);
						set(t[0], t[1], t[2], block::sand);
						break;
					}
				}
			}
		}
	}

}
