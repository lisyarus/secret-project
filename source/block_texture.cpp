#include <secret/block_texture.hpp>

#include <geom/primitives/ndarray.hpp>

#include <random>

namespace secret
{

	static constexpr std::size_t block_texture_size = 32;
	static constexpr std::size_t texture_count = 4;

	glow::texture make_block_texture ( )
	{
		std::default_random_engine rng(std::random_device{}());

		auto random_color_bias = [&rng](glow::color_4ub c)
		{
			std::uniform_real_distribution<float> gen(-1.f, 1.f);
			glow::color_4ub cc;

			float m = std::pow(2.f, gen(rng) * 0.125f);

			for (std::size_t i = 0; i < 3; ++i)
			{
				float q = c[i] * m;
				if (q < 0) q = 0;
				if (q > 255) q = 255;
				cc[i] = static_cast<unsigned char>(q);
			}

			cc[3] = c[3];
			return cc;
		};

		geom::ndarray<glow::color_4ub, 2> pixels({block_texture_size, block_texture_size * texture_count});

		for (std::size_t v = 0; v < block_texture_size; ++v)
			for (std::size_t u = 0; u < block_texture_size; ++u)
				pixels(u, v) = {0, 0, 0, 255};

		for (std::size_t v = block_texture_size; v < block_texture_size * 2; ++v)
			for (std::size_t u = 0; u < block_texture_size; ++u)
				pixels(u, v) = random_color_bias({127, 127, 127, 255});

		for (std::size_t v = block_texture_size * 2; v < block_texture_size * 3; ++v)
			for (std::size_t u = 0; u < block_texture_size; ++u)
				pixels(u, v) = random_color_bias({79, 31, 0, 255});

		for (std::size_t v = block_texture_size * 3; v < block_texture_size * 4; ++v)
			for (std::size_t u = 0; u < block_texture_size; ++u)
				pixels(u, v) = random_color_bias({223, 191, 0, 255});

		glow::texture block_texture;

		auto binder = block_texture.bind_scoped(gl::TEXTURE_2D_ARRAY)
			.parameter(gl::TEXTURE_MAG_FILTER, gl::NEAREST)
			.parameter(gl::TEXTURE_MIN_FILTER, gl::LINEAR_MIPMAP_LINEAR)
			.parameter(gl::TEXTURE_BASE_LEVEL, 0)
			.parameter(gl::TEXTURE_MAX_LEVEL, 5)
			.image_3d(0, {block_texture_size, block_texture_size, texture_count}, pixels.data())
			.generate_mipmap();

		return block_texture;
	}

	std::uint8_t texture_layer (block b)
	{
		switch (b)
		{
		case block::stone: return 1;
		case block::dirt : return 2;
		case block::sand : return 3;
		default          : return 0;
		}
	}

}
