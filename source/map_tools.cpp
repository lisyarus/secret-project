#include <secret/map_tools.hpp>

#include <secret/block.hpp>

#include <glow/object/buffer.hpp>
#include <glow/object/vertex_array.hpp>
#include <glow/object/program.hpp>

#include <functional>

static const char selection_program_vertex_source[] =
R"(#version 330

uniform mat4 u_transform;

layout (location = 0) in vec3 in_position;
layout (location = 1) in vec3 in_normal;
layout (location = 2) in vec4 in_color;

out vec4 color;
out vec3 position;
out vec3 normal;

void main ( )
{
	gl_Position = u_transform * vec4(in_position, 1.0);
	color = in_color;
	position = in_position;
	normal = in_normal;
}
)";

static const char selection_program_fragment_source[] =
R"(#version 330

in vec4 color;
in vec3 position;
in vec3 normal;

layout (location = 0) out vec4 out_color;
layout (location = 1) out vec3 out_normal;
layout (location = 2) out vec3 out_position;

void main ( )
{
	out_color = color;
	out_normal = normal * 0.5 + vec3(0.5, 0.5, 0.5);
	out_position = position;
}
)";

namespace
{

	struct vertex
	{
		geom::vector_3f position;
		geom::vector_3f normal;
		glow::color_4f color;
	};

	auto const attribs = glow::interleaved<geom::vector_3f, geom::vector_3f, glow::color_4f>();
}

namespace secret
{

	struct map_tools::implementation
	{
		glow::buffer vbo;
		glow::vertex_array vao;
		glow::program program;
		GLuint vertex_count;
		secret::world & world;

		std::function<void()> execute_callback;

		implementation (secret::world & w)
			: program(
				glow::vertex_shader(selection_program_vertex_source),
				glow::fragment_shader(selection_program_fragment_source)
			)
			, vertex_count{0}
			, world(w)
		{
			vbo.bind_scoped(gl::ARRAY_BUFFER)
				.with([&]{
					vao.bind_scoped()
						.attrib(0, std::get<0>(attribs))
						.attrib(1, std::get<1>(attribs))
						.attrib(2, std::get<2>(attribs))
						;
				});
		}

		void clear ( )
		{
			vertex_count = 0;
			execute_callback = nullptr;
		}

		void set_block_tool (geom::vector_3ul position, float size, block type, std::size_t radius, glow::color_4f color);
	};

	void map_tools::implementation::set_block_tool (geom::vector_3ul position, float size, block type, std::size_t radius, glow::color_4f color)
	{
		if (false
			|| (position[0] >= world.size()[0])
			|| (position[1] >= world.size()[1])
			|| (position[2] >= world.size()[2]))
		{
			clear();
			return;
		}

		execute_callback = [=]
		{
			int r = radius;
			for (int iz = -r; iz <= r; ++iz)
				for (int iy = -r; iy <= r; ++iy)
					for (int ix = -r; ix <= r; ++ix)
					{
						if (ix * ix + iy * iy + iz * iz > r * r) continue;

						std::size_t const x = position[0] + ix;
						std::size_t const y = position[1] + iy;
						std::size_t const z = position[2] + iz;

						if (x >= world.size()[0]) continue;
						if (y >= world.size()[1]) continue;
						if (z >= world.size()[2]) continue;

						world.set_block({x, y, z}, type);
					}
		};

		std::vector<vertex> quad_vertices;

		geom::vector_3f origin = geom::cast<float>(position);

		auto push_vertex = [&](int ix, int iy, int iz, int f)
		{
			static const geom::vector_3f h{0.5f, 0.5f, 0.5f};
			vertex v;
			v.position = origin + (geom::cast<float>(geom::vector_3i{ ix, iy, iz }) - h) * size + h;
			v.normal = face_normal(f);
			v.color = color;
			quad_vertices.push_back(v);
		};

		push_vertex(0, 0, 0, 0);
		push_vertex(0, 0, 1, 0);
		push_vertex(0, 1, 0, 0);
		push_vertex(0, 1, 1, 0);

		push_vertex(1, 0, 0, 1);
		push_vertex(1, 1, 0, 1);
		push_vertex(1, 0, 1, 1);
		push_vertex(1, 1, 1, 1);

		push_vertex(0, 0, 0, 2);
		push_vertex(1, 0, 0, 2);
		push_vertex(0, 0, 1, 2);
		push_vertex(1, 0, 1, 2);

		push_vertex(0, 1, 0, 3);
		push_vertex(0, 1, 1, 3);
		push_vertex(1, 1, 0, 3);
		push_vertex(1, 1, 1, 3);

		push_vertex(0, 0, 0, 4);
		push_vertex(0, 1, 0, 4);
		push_vertex(1, 0, 0, 4);
		push_vertex(1, 1, 0, 4);

		push_vertex(0, 0, 1, 5);
		push_vertex(1, 0, 1, 5);
		push_vertex(0, 1, 1, 5);
		push_vertex(1, 1, 1, 5);

		std::vector<vertex> vertices;

		for (std::size_t i = 0; i < quad_vertices.size(); i += 4)
		{
			vertices.push_back(quad_vertices[i + 0]);
			vertices.push_back(quad_vertices[i + 1]);
			vertices.push_back(quad_vertices[i + 2]);
			vertices.push_back(quad_vertices[i + 2]);
			vertices.push_back(quad_vertices[i + 1]);
			vertices.push_back(quad_vertices[i + 3]);
		}

		vbo.bind_scoped(gl::ARRAY_BUFFER).data(vertices, gl::STREAM_DRAW);
		vertex_count = vertices.size();
	}

	map_tools::map_tools (world & w)
		: pimpl_(std::make_unique<implementation>(w))
	{ }

	map_tools::~map_tools ( )
	{ }

	void map_tools::set_tool (no_tool)
	{
		impl().clear();
	}

	void map_tools::set_tool (place_block const & tool)
	{
		impl().set_block_tool(tool.position, 1.f, tool.type, tool.radius, {0.f, 1.f, 0.f, 0.25f});
	}

	void map_tools::set_tool (remove_block const & tool)
	{
		impl().set_block_tool(tool.position, 1.f + 1.f/64.f, block::air, tool.radius, {1.f, 0.f, 0.f, 0.25f});
	}

	void map_tools::render (geom::matrix_4x4f const & transform)
	{
		if (impl().vertex_count == 0)
			return;

		gl::Enable(gl::DEPTH_TEST);
		gl::Enable(gl::CULL_FACE);
		gl::Enablei(gl::BLEND, 0);
		gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA);

		impl().program.bind_scoped()
			.uniform("u_transform", transform)
			.with([&]{
				impl().vao.bind_scoped()
					.draw(gl::TRIANGLES, 0, impl().vertex_count);
			});

		gl::Disablei(gl::BLEND, 0);
		gl::Disable(gl::CULL_FACE);
		gl::Disable(gl::DEPTH_TEST);
	}

	void map_tools::execute ( )
	{
		auto f = std::move(impl().execute_callback);
		if (f)
			f();

		clear();
	}

}
