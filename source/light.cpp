#include <secret/light.hpp>

#include <queue>
#include <iostream>

namespace secret
{

	struct light::implementation
	{
		geom::ndarray<std::uint8_t, 3> lightmap;

		std::set<geom::vector_3ul> light_propagate;
		std::set<geom::vector_3ul> light_remove;
		std::set<geom::vector_3ul> sunlight_propagate;
		std::set<geom::vector_3ul> sunlight_remove;

		geom::ndarray<glow::texture, 3> light_textures;
		geom::ndarray<bool, 3> light_textures_need_update;

		implementation (block_map const & map);

		void update_light    (block_map const & blocks);
		void update_sunlight (block_map const & blocks);
		void update_textures ( );

		void set_von_neumann_neighbours_need_update (geom::vector_3ul const & p);

		std::uint8_t get_light (std::size_t x, std::size_t y, std::size_t z) const
		{
			return lightmap(x, y, z) & 0x0f;
		}

		void set_light (std::size_t x, std::size_t y, std::size_t z, std::uint8_t l)
		{
			std::uint8_t & v = lightmap(x, y, z);
			v = (v & 0xf0) | l;
		}

		std::uint8_t get_sunlight (std::size_t x, std::size_t y, std::size_t z) const
		{
			return (lightmap(x, y, z) & 0xf0) / 0x10;
		}

		void set_sunlight (std::size_t x, std::size_t y, std::size_t z, std::uint8_t l)
		{
			std::uint8_t & v = lightmap(x, y, z);
			v = (l * 0x10) | (v & 0x0f);
		}
	};

	light::implementation::implementation (block_map const & map)
		: lightmap(map.dimensions, 0)
		, light_textures(map.dimensions / chunk_size)
		, light_textures_need_update(map.dimensions / chunk_size, true)
	{
		for (std::size_t y = map.dimensions[1]; y --> 0;)
		{
			for (std::size_t z = 0; z < map.dimensions[2]; ++z)
			{
				for (std::size_t x = 0; x < map.dimensions[0]; ++x)
				{
					if (!transparent(map(x, y, z)))
						continue;

					if (y + 1 == map.dimensions[1])
					{
						set_sunlight(x, y, z, 0xf);
					}
					else if (get_sunlight(x, y + 1, z) == 0xf)
					{
						set_sunlight(x, y, z, 0xf);

						bool should_propagate = false;

						if (x > 0 && transparent(map(x-1, y, z)) && get_sunlight(x-1, y+1, z) != 0xf)
							should_propagate = true;

						if (x+1 < map.dimensions[0] && transparent(map(x+1, y, z)) && get_sunlight(x+1, y+1, z) != 0xf)
							should_propagate = true;

						if (z > 0 && transparent(map(x, y, z-1)) && get_sunlight(x, y+1, z-1) != 0xf)
							should_propagate = true;

						if (z+1 < map.dimensions[2] && transparent(map(x, y, z+1)) && get_sunlight(x, y+1, z+1) != 0xf)
							should_propagate = true;

						if (should_propagate)
							sunlight_propagate.insert({x, y, z});
					}
				}
			}
		}

		for (std::size_t z = 0; z < light_textures.dimensions[2]; ++z)
		{
			for (std::size_t y = 0; y < light_textures.dimensions[1]; ++y)
			{
				for (std::size_t x = 0; x < light_textures.dimensions[0]; ++x)
				{
					light_textures(x, y, z).bind(gl::TEXTURE_3D)
						.parameter(gl::TEXTURE_MAG_FILTER, gl::LINEAR)
						.parameter(gl::TEXTURE_MIN_FILTER, gl::LINEAR)
						.parameter(gl::TEXTURE_BASE_LEVEL, 0)
						.parameter(gl::TEXTURE_MAX_LEVEL, 0)
						.parameter(gl::TEXTURE_WRAP_S, gl::CLAMP_TO_EDGE)
						.parameter(gl::TEXTURE_WRAP_T, gl::CLAMP_TO_EDGE)
						.parameter(gl::TEXTURE_WRAP_R, gl::CLAMP_TO_EDGE);
				}
			}
		}

		glow::texture::null().bind(gl::TEXTURE_3D);
	}

	void light::implementation::update_light (block_map const & blocks)
	{
		{
			std::queue<geom::vector_3ul> light_remove_queue;

			for (auto p : light_remove)
				light_remove_queue.push(p);

			light_remove.clear();

			while (!light_remove_queue.empty())
			{
				geom::vector_3ul const p = light_remove_queue.front();

				set_von_neumann_neighbours_need_update(p);

				auto const x = p[0];
				auto const y = p[1];
				auto const z = p[2];

				light_remove_queue.pop();

				auto l = get_light(x, y, z);

				auto update = [&](std::size_t nx, std::size_t ny, std::size_t nz)
				{
					if (!transparent(blocks(nx, ny, nz)))
						return;

					auto k = get_light(nx, ny, nz);

					if (k > 0 && k < l)
					{
						light_remove_queue.push({nx, ny, nz});
					}
					else if (k >= l)
					{
						light_propagate.insert({nx, ny, nz});
					}
				};

				if (x > 0)
					update(x-1, y, z);

				if (x+1 < blocks.dimensions[0])
					update(x+1, y, z);

				if (y > 0)
					update(x, y-1, z);

				if (y+1 < blocks.dimensions[1])
					update(x, y+1, z);

				if (z > 0)
					update(x, y, z-1);

				if (z+1 < blocks.dimensions[2])
					update(x, y, z+1);

				set_light(x, y, z, 0);
			}
		}

		{
			std::queue<geom::vector_3ul> light_propagate_queue;

			for (auto p : light_propagate)
				light_propagate_queue.push(p);

			light_propagate.clear();

			while (!light_propagate_queue.empty())
			{
				geom::vector_3ul const p = light_propagate_queue.front();

				set_von_neumann_neighbours_need_update(p);

				auto const x = p[0];
				auto const y = p[1];
				auto const z = p[2];

				light_propagate_queue.pop();

				auto l = get_light(x, y, z);

				auto update = [&](std::size_t nx, std::size_t ny, std::size_t nz)
				{
					if (!transparent(blocks(nx, ny, nz)))
						return;

					if (get_light(nx, ny, nz) + 1 < l)
					{
						set_light(nx, ny, nz, l - 1);
						if (l > 1)
							light_propagate_queue.push({nx, ny, nz});
					}
				};

				if (x > 0)
					update(x-1, y, z);

				if (x+1 < blocks.dimensions[0])
					update(x+1, y, z);

				if (y > 0)
					update(x, y-1, z);

				if (y+1 < blocks.dimensions[1])
					update(x, y+1, z);

				if (z > 0)
					update(x, y, z-1);

				if (z+1 < blocks.dimensions[2])
					update(x, y, z+1);
			}
		}
	}

	void light::implementation::update_sunlight (block_map const & blocks)
	{
		{
			std::queue<geom::vector_3ul> sunlight_remove_queue;

			for (auto p : sunlight_remove)
				sunlight_remove_queue.push(p);

			sunlight_remove.clear();

			while (!sunlight_remove_queue.empty())
			{
				geom::vector_3ul const p = sunlight_remove_queue.front();

				set_von_neumann_neighbours_need_update(p);

				auto const x = p[0];
				auto const y = p[1];
				auto const z = p[2];

				sunlight_remove_queue.pop();

				auto l = get_sunlight(x, y, z);

				auto update = [&](std::size_t nx, std::size_t ny, std::size_t nz)
				{
					if (!transparent(blocks(nx, ny, nz)))
						return;

					auto k = get_sunlight(nx, ny, nz);

					if (k > 0 && k < l)
					{
						sunlight_remove_queue.push({nx, ny, nz});
					}
					else if (k >= l)
					{
						sunlight_propagate.insert({nx, ny, nz});
					}
				};

				if (x > 0)
					update(x-1, y, z);

				if (x+1 < blocks.dimensions[0])
					update(x+1, y, z);

				if (y+1 < blocks.dimensions[1])
					update(x, y+1, z);

				if (z > 0)
					update(x, y, z-1);

				if (z+1 < blocks.dimensions[2])
					update(x, y, z+1);

				if (y > 0)
				{
					if (l == 0xf)
					{
						if (transparent(blocks(x, y-1, z)))
							sunlight_remove_queue.push({x, y-1, z});
					}
					else
					{
						update(x, y-1, z);
					}
				}

				set_sunlight(x, y, z, 0);
			}
		}

		{
			std::queue<geom::vector_3ul> sunlight_propagate_queue;

			for (auto p : sunlight_propagate)
				sunlight_propagate_queue.push(p);

			sunlight_propagate.clear();

			while (!sunlight_propagate_queue.empty())
			{
				geom::vector_3ul const p = sunlight_propagate_queue.front();

				set_von_neumann_neighbours_need_update(p);

				auto const x = p[0];
				auto const y = p[1];
				auto const z = p[2];

				sunlight_propagate_queue.pop();

				auto l = get_sunlight(x, y, z);

				auto update = [&](std::size_t nx, std::size_t ny, std::size_t nz)
				{
					if (!transparent(blocks(nx, ny, nz)))
						return;

					if (get_sunlight(nx, ny, nz) + 1 < l)
					{
						set_sunlight(nx, ny, nz, l - 1);
						if (l > 1)
							sunlight_propagate_queue.push({nx, ny, nz});
					}
				};

				if (x > 0)
					update(x-1, y, z);

				if (x+1 < blocks.dimensions[0])
					update(x+1, y, z);

				if (y+1 < blocks.dimensions[1])
					update(x, y+1, z);

				if (z > 0)
					update(x, y, z-1);

				if (z+1 < blocks.dimensions[2])
					update(x, y, z+1);

				if (y > 0 && transparent(blocks(x, y-1, z)))
				{
					if (l == 0xf)
					{
						if (get_sunlight(x, y-1, z) < 0xf)
						{
							set_sunlight(x, y-1, z, 0xf);
							sunlight_propagate_queue.push({x, y-1, z});
						}
					}
					else
						update(x, y-1, z);
				}
			}
		}
	}

	void light::implementation::update_textures ( )
	{
		for (std::size_t cz = 0; cz < light_textures.dimensions[2]; ++cz)
		{
			for (std::size_t cy = 0; cy < light_textures.dimensions[1]; ++cy)
			{
				for (std::size_t cx = 0; cx < light_textures.dimensions[0]; ++cx)
				{
					if (light_textures_need_update(cx, cy, cz))
					{
						geom::ndarray<std::uint16_t, 3> chunk_lightmap({chunk_size+2, chunk_size+2, chunk_size+2});

						for (std::size_t z = 0; z < chunk_size + 2; ++z)
						{
							for (std::size_t y = 0; y < chunk_size + 2; ++y)
							{
								for (std::size_t x = 0; x < chunk_size + 2; ++x)
								{
									std::size_t const rz = cz * chunk_size + z;
									std::size_t const ry = cy * chunk_size + y;
									std::size_t const rx = cx * chunk_size + x;

									if (true
										&& rx > 0 && rx < lightmap.dimensions[0] + 1
										&& ry > 0 && ry < lightmap.dimensions[1] + 1
										&& rz > 0 && rz < lightmap.dimensions[2] + 1)
									{
										chunk_lightmap(x, y, z) = get_light(rx-1, ry-1, rz-1) * 0x10 + get_sunlight(rx-1, ry-1, rz-1) * 0x1000;
									}
									else
									{
										chunk_lightmap(x, y, z) = 0;
									};
								}
							}
						}

						light_textures(cx, cy, cz).bind(gl::TEXTURE_3D)
							.image_3d(0, gl::RG8, {chunk_size+2, chunk_size+2, chunk_size+2}, gl::RG, gl::UNSIGNED_BYTE, chunk_lightmap.data());

						light_textures_need_update(cx, cy, cz) = false;
					}
				}
			}
		}

		glow::texture::null().bind(gl::TEXTURE_3D);

		std::size_t light_total_data_size = light_textures.dimensions[0] * light_textures.dimensions[1] * light_textures.dimensions[2] * std::pow(chunk_size+2, 3) * 2;

//		std::cout << "Light: " << (light_total_data_size / 1024.f / 1024.f) << " MB" << std::endl;
	}

	void light::implementation::set_von_neumann_neighbours_need_update (geom::vector_3ul const & p)
	{
		auto const ch = p / chunk_size;

		auto border = [](std::size_t x, int t)
		{
			switch (t)
			{
			case -1: return (x % chunk_size) == 0;
			case 0: return true;
			case 1: return ((x + 1) % chunk_size) == 0;
			}
		};

		for (int tx : {-1, 0, 1})
		{
			for (int ty : {-1, 0, 1})
			{
				for (int tz : {-1, 0, 1})
				{
					std::size_t const cx = ch[0] + tx;
					std::size_t const cy = ch[1] + ty;
					std::size_t const cz = ch[2] + tz;

					if (true
						&& cx < light_textures.dimensions[0]
						&& cy < light_textures.dimensions[1]
						&& cz < light_textures.dimensions[2]
						&& border(p[0], tx)
						&& border(p[1], ty)
						&& border(p[2], tz)
						)
						light_textures_need_update(cx, cy, cz) = true;
				}
			}
		}
	}

	light::light (block_map const & map)
		: pimpl_(std::make_unique<implementation>(map))
	{ }

	light::~light ( )
	{ }

	void light::set_opaque (geom::vector_3ul const & pos)
	{
		impl().light_remove.insert(pos);
		impl().sunlight_remove.insert(pos);
	}

	void light::set_transparent (geom::vector_3ul const & pos)
	{
		std::size_t const x = pos[0];
		std::size_t const y = pos[1];
		std::size_t const z = pos[2];

		std::uint8_t l = 0, sl = 0;

		if (x > 0)
		{
			l = std::max<std::uint8_t>(l+1, impl().get_light(x-1, y, z))-1;
			sl = std::max<std::uint8_t>(sl+1, impl().get_sunlight(x-1, y, z))-1;
		}

		if (x < impl().lightmap.dimensions[0])
		{
			l = std::max<std::uint8_t>(l+1, impl().get_light(x+1, y, z))-1;
			sl = std::max<std::uint8_t>(sl+1, impl().get_sunlight(x+1, y, z))-1;
		}

		if (y > 0)
		{
			l = std::max<std::uint8_t>(l+1, impl().get_light(x, y-1, z))-1;
			sl = std::max<std::uint8_t>(sl+1, impl().get_sunlight(x, y-1, z))-1;
		}

		if (y < impl().lightmap.dimensions[1])
		{
			l = std::max<std::uint8_t>(l+1, impl().get_light(x, y+1, z))-1;
			if (impl().get_sunlight(x, y+1, z) == 0xf)
				sl = 0xf;
			else
				sl = std::max<std::uint8_t>(sl+1, impl().get_sunlight(x, y+1, z))-1;
		}

		if (z > 0)
		{
			l = std::max<std::uint8_t>(l+1, impl().get_light(x, y, z-1))-1;
			sl = std::max<std::uint8_t>(sl+1, impl().get_sunlight(x, y, z-1))-1;
		}

		if (z < impl().lightmap.dimensions[2])
		{
			l = std::max<std::uint8_t>(l+1, impl().get_light(x, y, z+1))-1;
			sl = std::max<std::uint8_t>(sl+1, impl().get_sunlight(x, y, z+1))-1;
		}

		if (y+1 == impl().lightmap.dimensions[1])
		{
			sl = 0xf;
		}

		impl().set_light(x, y, z, l);
		impl().set_sunlight(x, y, z, sl);

		impl().light_propagate.insert(pos);
		impl().sunlight_propagate.insert(pos);
	}

	void light::update (block_map const & blocks)
	{
		impl().update_light(blocks);
		impl().update_sunlight(blocks);
		impl().update_textures();
	}

	glow::texture & light::get_light_texture (geom::vector_3ul const & chunk) const
	{
		return impl().light_textures(chunk);
	}

}
