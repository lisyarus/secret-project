LEGEND:
* Planned
+ Implemented (variation)
- Declined (reasons)
! Having problems (which ones)



- A fixed-size voxel grid of sand & dirt & stone with a hill in the center
  (too vague)
* Chunk octree, six buffers per chunk (+x,-x,+y,-y,+z,-z), per-chunk frustum
  culling
* Basic sand physics: falls down if nothing underneath
* Raytracing for mouse picking, use octrees
* Stone & dirt physics: evaluate stress, breaks up if over threshold
* Villagers as red boxes, just walking around ignoring collisions
* Pathfinding for villagers in the same plane as the villager
* Pathfinding for villagers in 3D, can climb one block high & walk up stairs
