#pragma once

#include <geom/alias/vector.hpp>
#include <geom/alias/point.hpp>
#include <geom/alias/matrix.hpp>

namespace secret
{

	struct world_render_options
	{
		geom::matrix_4x4f modelview;
		geom::matrix_4x4f projection;
		geom::point_3f camera_pos;
		geom::point_3f camera_target;
		std::array<geom::vector_4f, 6> camera_clip_planes;
	};

}
