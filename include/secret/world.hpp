#pragma once

#include <secret/world_generator.hpp>
#include <secret/world_render_options.hpp>

#include <geom/alias/ray.hpp>

#include <variant>

namespace secret
{

	struct world
	{
		// size may be rounded above to chunk size
		world (geom::vector_3ul const & size, world_generator const & generator);

		~ world ( );

		geom::vector_3ul size ( ) const;

		void step (std::size_t tick_count, float dt);
		void render (world_render_options const & options);

		struct no_hit
		{ };

		struct terrain_hit
		{
			geom::vector_3ul block;
			std::uint8_t face;
		};

		using raycast_result = std::variant<no_hit, terrain_hit>;

		raycast_result raycast (geom::ray_3f const & ray) const;

		void set_block (geom::vector_3ul const & position, block b);

	private:
		struct implementation;
		std::unique_ptr<implementation> pimpl_;

		implementation       & impl ( )       { return *pimpl_; }
		implementation const & impl ( ) const { return *pimpl_; }
	};

}
