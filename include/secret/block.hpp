#pragma once

#include <geom/alias/vector.hpp>
#include <geom/primitives/ndarray.hpp>

namespace secret
{

	enum class block
	{
		air,
		stone,
		dirt,
		sand,

		terra_incognita = 255,
	};

	using block_map = geom::ndarray<block, 3>;

	inline constexpr std::size_t chunk_size = 32;

	inline constexpr bool transparent (block b)
	{
		switch (b)
		{
		case block::air:
			return true;
		default:
			return false;
		}
	}

	inline geom::vector_3i face_normal_i (std::size_t f)
	{
		geom::vector_3i n { 0, 0, 0 };
		n[f / 2] = (f % 2 == 0) ? -1 : 1;
		return n;
	}

	inline geom::vector_3f face_normal (std::size_t f)
	{
		return geom::cast<float>(face_normal_i(f));
	}

}
