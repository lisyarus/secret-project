#pragma once

#include <secret/block.hpp>
#include <secret/world_render_options.hpp>

#include <geom/alias/vector.hpp>
#include <geom/alias/point.hpp>
#include <geom/alias/rectangle.hpp>
#include <geom/alias/matrix.hpp>
#include <geom/alias/ray.hpp>

#include <memory>

namespace secret
{

	struct light;

	struct terrain
	{
		terrain (block_map blocks);
		~ terrain ( );

		void set (std::size_t x, std::size_t y, std::size_t z, block b);

		block get (std::size_t x, std::size_t y, std::size_t z) const
		{
			return blocks_(x, y, z);
		}

		block get (geom::vector_3ul const & b) const
		{
			return blocks_(b);
		}

		block_map const & blocks ( ) const
		{
			return blocks_;
		}

		void set_light (std::size_t x, std::size_t y, std::size_t z);

		void remove_light (std::size_t x, std::size_t y, std::size_t z);

		struct raycast_result
		{
			geom::vector_3ul block;
			std::uint8_t face;
		};

		std::optional<raycast_result> raycast (geom::ray_3f const & ray) const;

		void render (world_render_options const & options, secret::light const & light);

		void step (std::size_t tick_count);

	private:
		block_map blocks_; // stored outside pimpl for faster access

		struct implementation;
		std::unique_ptr<implementation> pimpl_;

		implementation & impl ( ) const { return *pimpl_; }
	};

}
