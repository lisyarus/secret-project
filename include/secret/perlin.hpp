#pragma once

#include <geom/primitives/ndarray.hpp>
#include <geom/alias/vector.hpp>

#include <random>

namespace secret
{

	// A 2D map with values in [-1, 1]
	geom::ndarray<float, 2> perlin (std::default_random_engine & rng,
		geom::vector_2ul const & block_count, geom::vector_2ul const & block_size,
		bool repeatable = false);

	inline geom::ndarray<float, 2> perlin (std::default_random_engine & rng,
		geom::vector_2ul const & block_count, std::size_t block_size,
		bool repeatable = false)
	{
		return perlin(rng, block_count, {block_size, block_size}, repeatable);
	}

}
