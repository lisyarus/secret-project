#pragma once

#include <secret/block.hpp>

#include <geom/alias/vector.hpp>

#include <glow/object/texture.hpp>

namespace secret
{

	glow::texture make_block_texture ( );

	std::uint8_t texture_layer (block b);

}
