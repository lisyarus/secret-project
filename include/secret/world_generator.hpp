#pragma once

#include <secret/block.hpp>

#include <memory>

namespace secret
{

	struct world_generator
	{
		// size in terrain chunks
		virtual block_map generate_terrain (geom::vector_3ul const & size) const = 0;

		virtual ~ world_generator ( ) { }
	};

	std::unique_ptr<world_generator> make_default_world_generator ( );

}
