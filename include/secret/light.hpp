#pragma once

#include <secret/block.hpp>

#include <glow/object/texture.hpp>

#include <memory>
#include <set>

namespace secret
{

	struct light
	{
		light (block_map const & map);
		~ light ( );

		void set_transparent (geom::vector_3ul const & pos);
		void set_opaque      (geom::vector_3ul const & pos);

		void update (block_map const & blocks);

		glow::texture & get_light_texture (geom::vector_3ul const & chunk) const;

	private:
		struct implementation;
		std::unique_ptr<implementation> pimpl_;

		implementation & impl ( ) const { return *pimpl_; }
	};

}
