#pragma once

#include <secret/world.hpp>

#include <memory>
#include <geom/alias/point.hpp>
#include <geom/alias/matrix.hpp>
#include <glow/color.hpp>

namespace secret
{

	struct map_tools
	{
		map_tools (world & w);
		~ map_tools ( );

		struct no_tool
		{ };

		struct place_block
		{
			geom::vector_3ul position;
			block type;
			std::size_t radius = 1;
		};

		struct remove_block
		{
			geom::vector_3ul position;
			std::size_t radius = 1;
		};

		void set_tool (no_tool);
		void set_tool (place_block const & tool);
		void set_tool (remove_block const & tool);

		void clear ( ) { set_tool(no_tool{}); }

		void render (geom::matrix_4x4f const & transform);
		void execute ( );

	private:
		struct implementation;
		std::unique_ptr<implementation> pimpl_;

		implementation & impl ( ) { return *pimpl_; }
	};

}
