#pragma once

#include <secret/block.hpp>

#include <geom/alias/matrix.hpp>

#include <memory>

namespace secret
{

	struct grass
	{
		grass (block_map const & map);
		~ grass ( );

		void render (geom::matrix_4x4f const & transform);

	private:
		struct implementation;
		std::unique_ptr<implementation> pimpl_;

		implementation & impl ( ) const { return *pimpl_; }
	};

}
